/*
 *  Authors:
 *    Gustavo Gutierrez-Sabogal <gustavo.ggutierrez@gmail.com>
 *
 *  Copyright:
 *    Gustavo Gutierrez, 2013
 *
 *  Permission is hereby granted, free of charge, to any person obtaining
 *  a copy of this software, to deal in the software without restriction,
 *  including without limitation the rights to use, copy, modify, merge,
 *  publish, distribute, sublicense, and/or sell copies of the software,
 *  and to permit persons to whom the software is furnished to do so, subject
 *  to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 *  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 *  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

using Gecode::AFC;
using Gecode::Activity;
using Gecode::ViewArray;
using Gecode::Rnd;
using Gecode::VarBranch;
using Gecode::ValBranch;
using Gecode::BranchTbl;
using Gecode::VoidFunction;
using Gecode::MeritBase;
using Gecode::ViewSel;
using Gecode::ViewSelMaxTbl;
using Gecode::ViewSelMinTbl;
using Gecode::ViewSelMax;
using Gecode::ViewSelMin;
using Gecode::ViewSelNone;
using Gecode::ViewSelRnd;
using Gecode::MeritFunction;
using Gecode::MeritDegree;
using Gecode::MeritActivity;
using Gecode::ValSel;
using Gecode::ValCommit;
using Gecode::ValSelCommit;
using Gecode::ValSelCommitBase;
using Gecode::ValSelFunction;
using Gecode::ValCommitFunction;
using Gecode::ViewValBrancher;
using Gecode::TieBreak;
using Gecode::function_cast;
using Gecode::BrancherHandle;
using Gecode::NGL;

using CuddAbstraction::NumericTuple;

namespace MPG {
namespace Rel {
class UnknownBranching : public Exception {
public:
  UnknownBranching(const char* l)
      : Exception(l, "Unknown branching (variable or value)") {}
};
}
}

// branching
// branch function types
namespace MPG {
typedef bool (*RelBranchFilter)(const Space& home, RelVar x, int i);
typedef void (*RelVarValPrint)(const Space& home, const BrancherHandle& bh,
                               unsigned int a, RelVar x, int i,
                               const Relation& n, std::ostream& os);
typedef double (*RelBranchMerit)(const Space& home, RelVar x, int i);
typedef Relation (*RelBranchVal)(const Space& home, RelVar x, int i);
typedef void (*RelBranchCommit)(Space& home, unsigned int a, RelVar x, int i,
                                Relation n);
}
// branch traits
namespace Gecode {
template <>
class BranchTraits<MPG::RelVar> {
public:
  typedef MPG::RelBranchFilter Filter;
  typedef MPG::RelBranchMerit Merit;
  typedef MPG::RelBranchVal Val;
  typedef Relation ValType;
  typedef MPG::RelBranchCommit Commit;
};
}

// variable AFC
namespace MPG {
class IntAFC : public AFC {
public:
  IntAFC(void);
  IntAFC(const IntAFC& a);
  IntAFC& operator=(const IntAFC& a);
  IntAFC(Home home, const RelVarArgs& x, double d = 1.0);
  void init(Home home, const RelVarArgs& x, double d = 1.0);
};
inline IntAFC::IntAFC(void) {}
inline IntAFC::IntAFC(const IntAFC& a) : AFC(a) {}
inline IntAFC& IntAFC::operator=(const IntAFC& a) {
  return static_cast<IntAFC&>(AFC::operator=(a));
}
inline IntAFC::IntAFC(Home home, const RelVarArgs& x, double d) {
  AFC::init(home, x, d);
}
inline void IntAFC::init(Home home, const RelVarArgs& x, double d) {
  AFC::init(home, x, d);
}
}

// variable activity
namespace MPG {
class RelActivity : public Activity {
public:
  RelActivity(void);
  RelActivity(const RelActivity& a);
  RelActivity& operator=(const RelActivity& a);
  RelActivity(Home home, const RelVarArgs& x, double d = 1.0,
              RelBranchMerit bm = nullptr);
  void init(Home home, const RelVarArgs& x, double d = 1.0,
            RelBranchMerit bm = nullptr);
};
inline RelActivity::RelActivity(void) {}
inline RelActivity::RelActivity(const RelActivity& a) : Activity(a) {}
inline RelActivity& RelActivity::operator=(const RelActivity& a) {
  return static_cast<RelActivity&>(Activity::operator=(a));
}
inline RelActivity::RelActivity(Home home, const RelVarArgs& x, double d,
                                RelBranchMerit bm) {
  ViewArray<Rel::RelView> y(home, x);
  Activity::init(home, y, d, bm);
}
inline void RelActivity::init(Home home, const RelVarArgs& x, double d,
                              RelBranchMerit bm) {
  ViewArray<Rel::RelView> y(home, x);
  Activity::init(home, y, d, bm);
}
}

namespace MPG {
// variable selection class
class RelVarBranch : public VarBranch {
public:
  enum Select {
    SEL_NONE,
    SEL_RND,
    SEL_MERIT_MAX,
    SEL_DEGREE_MAX,
    SEL_ACTIVITY_MAX,
    SEL_SIZE_MIN
  };

protected:
  Select s;

public:
  RelVarBranch(void);
  RelVarBranch(Rnd r);
  RelVarBranch(Select s0, BranchTbl t);
  RelVarBranch(Select s0, double d, BranchTbl t);
  RelVarBranch(Select s0, Activity a, BranchTbl t);
  RelVarBranch(Select s0, VoidFunction mf, BranchTbl t);
  Select select(void) const;
  // expand activity
  void expand(Home home, const RelVarArgs& x) {
    if ((select() == SEL_ACTIVITY_MAX) && !activity().initialized())
      activity(RelActivity(home, x, decay()));
  }
};
inline RelVarBranch::RelVarBranch(void)
    : VarBranch(NULL)
    , s(SEL_NONE) {}
inline RelVarBranch::RelVarBranch(Rnd r)
    : VarBranch(r)
    , s(SEL_RND) {}
inline RelVarBranch::RelVarBranch(Select s0, BranchTbl t)
    : VarBranch(t)
    , s(s0) {}
inline RelVarBranch::RelVarBranch(Select s0, double d, BranchTbl t)
    : VarBranch(d, t)
    , s(s0) {}
inline RelVarBranch::RelVarBranch(Select s0, Activity a, BranchTbl t)
    : VarBranch(a, t)
    , s(s0) {}
inline RelVarBranch::RelVarBranch(Select s0, VoidFunction mf, BranchTbl t)
    : VarBranch(mf, t)
    , s(s0) {}
inline RelVarBranch::Select RelVarBranch::select(void) const { return s; }
// variable selection functions
RelVarBranch REL_VAR_NONE(void);
RelVarBranch REL_VAR_RND(Rnd r);
RelVarBranch REL_VAR_MERIT_MAX(RelBranchMerit bm, BranchTbl tbl = NULL);
RelVarBranch REL_VAR_DEGREE_MAX(BranchTbl tbl = NULL);
RelVarBranch REL_VAR_ACTIVITY_MAX(double d = 1.0, BranchTbl tbl = NULL);
RelVarBranch REL_VAR_ACTIVITY_MAX(RelActivity a, BranchTbl tbl = NULL);
RelVarBranch REL_VAR_SIZE_MIN(BranchTbl tbl = NULL);
// variable selection function implementation
inline RelVarBranch REL_VAR_MERIT_MAX(RelBranchMerit bm, BranchTbl tbl) {
  return RelVarBranch(RelVarBranch::SEL_MERIT_MAX,
                      function_cast<VoidFunction>(bm), tbl);
}
inline RelVarBranch REL_VAR_NONE(void) {
  return RelVarBranch(RelVarBranch::SEL_NONE, NULL);
}
inline RelVarBranch REL_VAR_RND(Rnd r) { return RelVarBranch(r); }
inline RelVarBranch REL_VAR_DEGREE_MAX(BranchTbl tbl) {
  return RelVarBranch(RelVarBranch::SEL_DEGREE_MAX, tbl);
}
inline RelVarBranch REL_VAR_ACTIVITY_MAX(double d, BranchTbl tbl) {
  return RelVarBranch(RelVarBranch::SEL_ACTIVITY_MAX, d, tbl);
}
inline RelVarBranch REL_VAR_ACTIVITY_MAX(RelActivity a, BranchTbl tbl) {
  return RelVarBranch(RelVarBranch::SEL_ACTIVITY_MAX, a, tbl);
}
inline RelVarBranch REL_VAR_SIZE_MIN(BranchTbl tbl) {
  return RelVarBranch(RelVarBranch::SEL_SIZE_MIN, tbl);
}
// value selection functions
class RelValBranch : public ValBranch {
public:
  enum Select {
    SEL_MIN,
    SEL_RND,
    SEL_VAL_COMMIT
  };

protected:
  Select s;

public:
  RelValBranch(Select s0 = SEL_MIN) : s(s0) {}
  RelValBranch(Rnd r)
      : ValBranch(r)
      , s(SEL_RND) {}
  RelValBranch(VoidFunction v, VoidFunction c)
      : ValBranch(v, c)
      , s(SEL_VAL_COMMIT) {}
  Select select(void) const { return s; }
};
RelValBranch REL_VAL_MIN(void);
RelValBranch REL_VAL_RND(Rnd r);
RelValBranch REL_VAL(RelBranchVal v, RelBranchCommit c = NULL);
inline RelValBranch REL_VAL_MIN(void) {
  return RelValBranch(RelValBranch::SEL_MIN);
}
inline RelValBranch REL_VAL_RND(Rnd r) { return RelValBranch(r); }
inline RelValBranch REL_VAL(RelBranchVal v, RelBranchCommit c) {
  return RelValBranch(function_cast<VoidFunction>(v),
                      function_cast<VoidFunction>(c));
}
}

namespace MPG {
namespace Rel {
// view selection creation function
// size merit class
class MeritSize : public MeritBase<RelView, double> {
public:
  MeritSize(Space& home, const VarBranch& vb)
      : MeritBase<RelView, double>(home, vb) {}
  MeritSize(Space& home, bool share, MeritSize& m)
      : MeritBase<RelView, double>(home, share, m) {}
  double operator()(const Space& home, RelView x, int i) {
    return x.lub().cardinality() - x.glb().cardinality();
  }
};
inline ViewSel<RelView>* viewsel(Space& home, const RelVarBranch& ivb) {
  if (ivb.select() == RelVarBranch::SEL_NONE)
    return new (home) ViewSelNone<RelView>(home, ivb);
  if (ivb.select() == RelVarBranch::SEL_RND)
    return new (home) ViewSelRnd<RelView>(home, ivb);
  if (ivb.tbl() != NULL) {
    // view selection with tbl-function
    switch (ivb.select()) {
    case RelVarBranch::SEL_MERIT_MAX:
      return new (home) ViewSelMaxTbl<MeritFunction<RelView>>(home, ivb);
    case RelVarBranch::SEL_DEGREE_MAX:
      return new (home) ViewSelMaxTbl<MeritDegree<RelView>>(home, ivb);
    case RelVarBranch::SEL_ACTIVITY_MAX:
      return new (home) ViewSelMaxTbl<MeritActivity<RelView>>(home, ivb);
    case RelVarBranch::SEL_SIZE_MIN:
      return new (home) ViewSelMinTbl<MeritSize>(home, ivb);
    default:
      ;
    }
  } else {
    // view selection without tbl-function
    switch (ivb.select()) {
    case RelVarBranch::SEL_MERIT_MAX:
      return new (home) ViewSelMax<MeritFunction<RelView>>(home, ivb);
    case RelVarBranch::SEL_DEGREE_MAX:
      return new (home) ViewSelMax<MeritDegree<RelView>>(home, ivb);
    case RelVarBranch::SEL_ACTIVITY_MAX:
      return new (home) ViewSelMax<MeritActivity<RelView>>(home, ivb);
    case RelVarBranch::SEL_SIZE_MIN:
      return new (home) ViewSelMin<MeritSize>(home, ivb);
    default:
      ;
    }
  }
  throw UnknownBranching("Rel::branch");
}
// value selection and commit creation function
// value selection classes
class ValSelMin : public ValSel<RelView, Relation> {
public:
  ValSelMin(Space& home, const ValBranch& vb)
      : ValSel<RelView, Relation>(home, vb) {}
  ValSelMin(Space& home, bool share, ValSelMin& vs)
      : ValSel<RelView, Relation>(home, share, vs) {}
  Relation val(const Space& home, RelView x, int i) {
    Relation unknown = x.lub().differenceWith(x.glb());
    Relation r = unknown.pickAnyUnarySubrelation(getRelationSpace());
    return r;
  }
};
class ValSelRnd : public ValSel<RelView, Relation> {
protected:
  Rnd r;

public:
  ValSelRnd(Space& home, const ValBranch& vb)
      : ValSel<RelView, Relation>(home, vb)
      , r(vb.rnd()) {}
  ValSelRnd(Space& home, bool share, ValSelRnd& vs)
      : ValSel<RelView, Relation>(home, share, vs) {
    r.update(home, share, vs.r);
  }
  Relation val(const Space& home, RelView x, int i) {
    Relation unknown = x.lub().differenceWith(x.glb());
    Relation r = unknown.pickAnyUnarySubrelation(getRelationSpace());
    return r;
  }
  bool notice(void) const { return true; }
  void dispose(Space& home) { r.~Rnd(); }
};
// value commit classes
class ValCommitLq : public ValCommit<RelView, Relation> {
public:
  ValCommitLq(Space& home, const ValBranch& vb)
      : ValCommit<RelView, Relation>(home, vb) {}
  ValCommitLq(Space& home, bool share, ValCommitLq& vc)
      : ValCommit<RelView, Relation>(home, share, vc) {}
  ModEvent commit(Space& home, unsigned int a, RelView x, int i, Relation r) {
    return (a == 0) ? x.include(home, r) : x.exclude(home, r);
  }
  void print(const Space& home, unsigned int a, RelView x, int i, Relation n,
             std::ostream& o) const {
    // TODO: for now we only use tuple inclusion/exclusion. This has to be
    // more generic.
    assert(n.cardinality() == 1);
    CuddAbstraction::NumericTuple t = n.pickRandomTuple();

    o << "x[" << i << "]" << ((a == 0) ? "incl: " : "excl: ") << " " << t;
  }
  // no-good literal creation
  NGL* ngl(Space& home, unsigned int a, RelView x, Relation r) const {
    // if (a == 0)
    //   return new (home) LqNGL(home, x, n);
    // else
    return NULL;
  }
};
inline ValSelCommitBase<RelView, Relation>*
valselcommit(Space& home, const RelValBranch& ivb) {
  switch (ivb.select()) {
  case RelValBranch::SEL_MIN:
    return new (home) ValSelCommit<ValSelMin, ValCommitLq>(home, ivb);
  case RelValBranch::SEL_RND:
    return new (home) ValSelCommit<ValSelRnd, ValCommitLq>(home, ivb);
  case RelValBranch::SEL_VAL_COMMIT:
    // user-defined value selection and commit functions
    if (ivb.commit() == NULL) {
      return new (home) ValSelCommit<ValSelFunction<RelView>, ValCommitLq>(home,
                                                                           ivb);
    } else {
      return new (home)
          ValSelCommit<ValSelFunction<RelView>, ValCommitFunction<RelView>>(
              home, ivb);
    }
  default:
    throw UnknownBranching("Rel::branch");
  }
}
}
}

namespace Gecode {
/// Writes a representation of the relation \a r in \a e
inline Gecode::Archive& operator<<(Gecode::Archive& e, const Relation& r) {
  // Encoding: arity # t0 # ... # tn-1
  assert(false && "Not implemented yet");
  // Assumes r has only one tuple
  assert(r.cardinality() == 1);

  return e;
}
inline Gecode::Archive& operator>>(Gecode::Archive& e, Relation& r) {
  // Encoding: n # t0 # ... # tn-1
  assert(false && "Not implemented yet");
  return e;
}
}

namespace MPG {
// branch function
inline BrancherHandle branch(Home home, const RelVarArgs& x, RelVarBranch vars,
                             RelValBranch vals, RelBranchFilter bf = NULL,
                             RelVarValPrint vvp = NULL) {
  using namespace Rel;
  if (home.failed())
    return BrancherHandle();
  vars.expand(home, x);
  ViewArray<RelView> xv(home, x);
  ViewSel<RelView>* vs[1] = {viewsel(home, vars)};
  return ViewValBrancher<RelView, 1, Relation, 2>::post(
      home, xv, vs, valselcommit(home, vals), bf, vvp);
}

inline BrancherHandle branch(Home home, RelVar x, RelValBranch val) {
  RelVarArgs va(1);
  va[0] = x;
  return branch(home, va, REL_VAR_NONE(), val);
}
// branch function with tie-breaking
// inline BrancherHandle
// branch(Home home, const RelVarArgs& x,
//        TieBreak<IntVarBranch> vars, IntValBranch vals,
//        IntBranchFilter bf=NULL) {
//   using namespace Rel;
//   if (home.failed()) return BrancherHandle();
//   vars.a.expand(home,x);
//   // normalizing tie-breaking
//   if ((vars.a.select() == IntVarBranch::SEL_NONE) ||
//       (vars.a.select() == IntVarBranch::SEL_RND))
//     vars.b = INT_VAR_NONE();
//   vars.b.expand(home,x);
//   if ((vars.b.select() == IntVarBranch::SEL_NONE) ||
//       (vars.b.select() == IntVarBranch::SEL_RND))
//     vars.c = INT_VAR_NONE();
//   vars.c.expand(home,x);
//   if ((vars.c.select() == IntVarBranch::SEL_NONE) ||
//       (vars.c.select() == IntVarBranch::SEL_RND))
//     vars.d = INT_VAR_NONE();
//   vars.d.expand(home,x);
//   ViewArray<IntView> xv(home,x);
//   if (vars.b.select() == IntVarBranch::SEL_NONE) {
//     ViewSel<IntView>* vs[1] = {
//       viewsel(home,vars.a)
//     };
//     return ViewValBrancher<IntView,1,int,2>
//       ::post(home,xv,vs,valselcommit(home,vals),bf);
//   } else if (vars.c.select() == IntVarBranch::SEL_NONE) {
//     ViewSel<IntView>* vs[2] = {
//       viewsel(home,vars.a), viewsel(home,vars.b)
//     };
//     return ViewValBrancher<IntView,2,int,2>
//       ::post(home,xv,vs,valselcommit(home,vals),bf);
//   } else if (vars.d.select() == IntVarBranch::SEL_NONE) {
//     ViewSel<IntView>* vs[3] = {
//       viewsel(home,vars.a), viewsel(home,vars.b),
//       viewsel(home,vars.c)
//     };
//     return ViewValBrancher<IntView,3,int,2>
//       ::post(home,xv,vs,valselcommit(home,vals),bf);
//   } else {
//     ViewSel<IntView>* vs[4] = {
//       viewsel(home,vars.a), viewsel(home,vars.b),
//       viewsel(home,vars.c), viewsel(home,vars.d)
//     };
//     return ViewValBrancher<IntView,4,int,2>
//       ::post(home,xv,vs,valselcommit(home,vals),bf);
//   }
// }
}
