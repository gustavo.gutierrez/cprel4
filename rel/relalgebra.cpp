/*
 *  Authors:
 *    Gustavo Gutierrez-Sabogal <gustavo.ggutierrez@gmail.com>
 *
 *  Copyright:
 *    Gustavo Gutierrez, 2013
 *
 *  Permission is hereby granted, free of charge, to any person obtaining
 *  a copy of this software, to deal in the software without restriction,
 *  including without limitation the rights to use, copy, modify, merge,
 *  publish, distribute, sublicense, and/or sell copies of the software,
 *  and to permit persons to whom the software is furnished to do so, subject
 *  to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 *  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 *  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#include "rel.hh"

namespace MPG {
using Rel::RelView;
using Rel::ComplementView;
using CuddAbstraction::Schema;

///////////////////////////////
// Projection post function  //
///////////////////////////////
void projection(Space& home, RelVar x, const Schema& a, RelVar y) {
  if (home.failed())
    return;

  if (!CuddAbstraction::projectCompatible(x.schema(), a, y.schema())) {
    throw Rel::ProjectionIncompatible("projection");
  }

  RelView vx(x), vy(y);
  if (Rel::Projection<RelView, RelView>::post(home, vx, a, vy) != ES_OK)
    home.fail();
}

////////////////////////
// Join post function //
////////////////////////
void join(Space& home, RelVar x, RelVar y, RelVar z) {
  if (home.failed())
    return;

  using namespace CuddAbstraction;

  auto& relHome = Rel::getRelationSpace();
  if (!joinCompatible(relHome, x.schema(), y.schema(), z.schema())) {
    throw Rel::JoinIncompatible("join");
  }

  RelView vx(x), vy(y), vz(z);
  if (Rel::Join<RelView, RelView, RelView>::post(home, vx, vy, vz) != ES_OK)
    home.fail();
}

//////////////////////////////
// Compose posting function //
//////////////////////////////
void compose(Space& home, RelVar x, RelVar y, RelVar z) {
  if (home.failed())
    return;

  using namespace CuddAbstraction;

  auto& relHome = Rel::getRelationSpace();
  if (!composeCompatible(relHome, x.schema(), y.schema(), z.schema())) {
    std::cerr << "Compose incompatible" << std::endl;
    throw Rel::ComposeIncompatible("join");
  }

  /**
   * The composition constraint z = \compose{x}{y} is enforced by two
   * constraints:
   *
   * z' = \join{x}{y}
   * z = \proj{\attributes{z}}{z'}
   *
   * Where z' is a temporary variable on schema x.schema() \cup y.schema{}
   */
  const Schema& xSch = x.schema();
  const Schema& ySch = y.schema();
  Schema joinSch = xSch.unionWith(relHome, ySch);
  RelVar zp(home, joinSch);
  join(home, x, y, zp);
  projection(home, zp, z.schema(), z);
}

//////////////////////////
// Rename post function //
//////////////////////////
void rename(Space& home, RelVar x, const AttributeMap& m, RelVar y) {
  if (home.failed())
    return;
  // TODO: throw an exception instead
  assert(m.mappingBetween(x.schema(), y.schema()));
  RelView vx(x), vy(y);
  if (Rel::Rename<RelView, RelView>::post(home, vx, m, vy) != ES_OK)
    home.fail();
}
}