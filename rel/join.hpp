/*
 *  Authors:
 *    Gustavo Gutierrez-Sabogal <gustavo.ggutierrez@gmail.com>
 *
 *  Copyright:
 *    Gustavo Gutierrez, 2013
 *
 *  Permission is hereby granted, free of charge, to any person obtaining
 *  a copy of this software, to deal in the software without restriction,
 *  including without limitation the rights to use, copy, modify, merge,
 *  publish, distribute, sublicense, and/or sell copies of the software,
 *  and to permit persons to whom the software is furnished to do so, subject
 *  to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 *  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 *  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#include "rel.hh"

using Gecode::ExecStatus;
using Gecode::PropCost;
using Gecode::ModEventDelta;
using Gecode::ES_OK;
using Gecode::ES_NOFIX;
using Gecode::ES_FIX;

// Use the combined form of join and project
#define ABSTRACTION_USE_JOINPROJECT 1

namespace MPG {
namespace Rel {

template <class View0, class View1, class View2>
class Join : public Propagator {
protected:
  View0 x;
  View1 y;
  View2 z;

public:
  /////////////
  // posting //
  /////////////
  Join(Space& home, View0 x0, View1 y0, View2 z0)
      : Propagator(home)
      , x(x0)
      , y(y0)
      , z(z0) {
    x.subscribe(home, *this, PC_REL_ANY);
    y.subscribe(home, *this, PC_REL_ANY);
    z.subscribe(home, *this, PC_REL_ANY);
  }
  static ExecStatus post(Space& home, View0 x0, View1 y0, View2 z0) {
    (void)new (home) Join(home, x0, y0, z0);
    getCPRelStats().joinPropPosts++;
    return ES_OK;
  }
  //////////////
  // disposal //
  //////////////
  virtual size_t dispose(Space& home) {
    x.cancel(home, *this, PC_REL_ANY);
    y.cancel(home, *this, PC_REL_ANY);
    z.cancel(home, *this, PC_REL_ANY);
    (void)Propagator::dispose(home);
    return sizeof(*this);
  }
  /////////////
  // copying //
  /////////////
  Join(Space& home, bool share, Join& p)
      : Propagator(home, share, p)
      , x(p.x)
      , y(p.y)
      , z(p.z) {
    x.update(home, share, p.x);
    y.update(home, share, p.y);
    z.update(home, share, p.z);
  }
  virtual Propagator* copy(Space& home, bool share) {
    return new (home) Join(home, share, *this);
  }
  //////////////////////
  // cost computation //
  //////////////////////
  virtual PropCost cost(const Space&, const ModEventDelta&) const {
    return PropCost::binary(PropCost::LO);
  }
  /////////////////
  // propagation //
  /////////////////
  virtual ExecStatus propagate(Space& home, const ModEventDelta&) {
    getCPRelStats().joinPropExecutions++;

    /////////////
    // Prune Z //
    /////////////
    GECODE_ME_CHECK(z.include(home, (x.glb() * y.glb())));

    GECODE_ME_CHECK(z.exclude(home, !(x.lub() * y.lub())));

    /////////////
    // Prune X //
    /////////////
    GECODE_ME_CHECK(x.include(home, (z.glb() | x.schema())));

#ifdef ABSTRACTION_USE_JOINPROJECT
    Relation noSupportY = y.glb().joinWithAndProject(
        getRelationSpace(), z.lub().complement(), x.schema());
    GECODE_ME_CHECK(x.exclude(home, noSupportY));
#else
    Relation joinYglbZoob = y.glb() * !z.lub();
    Relation noSupportY = joinYglbZoob | x.schema();
    GECODE_ME_CHECK(x.exclude(home, noSupportY));
#endif

    /////////////
    // Prune Y //
    /////////////
    GECODE_ME_CHECK(y.include(home, (z.glb() | y.schema())));
#ifdef ABSTRACTION_USE_JOINPROJECT
    Relation noSupportX = x.glb().joinWithAndProject(
        getRelationSpace(), z.lub().complement(), y.schema());
    GECODE_ME_CHECK(y.exclude(home, noSupportX));
#else
    Relation joinXglbZoob = x.glb() * !z.lub();
    Relation noSupportX = joinXglbZoob | y.schema();
    GECODE_ME_CHECK(y.exclude(home, noSupportX));
#endif
    ///////////////
    // Fix point //
    ///////////////
    if (x.assigned() && y.assigned() && z.assigned())
      return home.ES_SUBSUMED(*this);

    return ES_FIX;
  }
};
}
}
