#include <gecode/gist.hh>

namespace MPG {
namespace Rel {
template <typename S>
class PrintDomainGroup : public Gecode::Gist::TextOutput,
                         public Gecode::Gist::Inspector {
private:
  /// TODO: This reference to the domain group should be constant. The problem
  // is inside the CuddAbstraction.
  CuddAbstraction::DomainGroup& dg_;

public:
  PrintDomainGroup(const std::string& name, CuddAbstraction::DomainGroup& dg)
      : Gecode::Gist::TextOutput(name)
      , dg_(dg) {}
  virtual void inspect(const Space& node) {
    init();
    addHtml("<pre>\n");
    dynamic_cast<const S&>(node).print(getStream(), dg_);
    flush();
    addHtml("</pre><hr />");
  }
  virtual std::string name(void) { return TextOutput::name(); }
  virtual void finalize(void) { TextOutput::finalize(); }
};
}
}