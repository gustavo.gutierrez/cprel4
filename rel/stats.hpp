/*
 *  Authors:
 *    Gustavo Gutierrez-Sabogal <gustavo.ggutierrez@gmail.com>
 *
 *  Copyright:
 *    Gustavo Gutierrez, 2013
 *
 *  Permission is hereby granted, free of charge, to any person obtaining
 *  a copy of this software, to deal in the software without restriction,
 *  including without limitation the rights to use, copy, modify, merge,
 *  publish, distribute, sublicense, and/or sell copies of the software,
 *  and to permit persons to whom the software is furnished to do so, subject
 *  to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 *  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 *  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */
namespace MPG {
namespace Rel {
class CPRelStats {
  ////////////////
  // Projection //
  ////////////////
public:
  /// Number of projection constraints posted
  unsigned int projPropPosts;
  /// Number of projection constraints that can use fast unique abstraction
  unsigned int projPropPostsFastUnique;
  /// Number of executions for the projection propagator
  unsigned int projPropExecutions;

  //////////
  // Join //
  //////////
public:
  /// Number of join constraints posted
  unsigned int joinPropPosts;
  /// Number of executions for the join propagator
  unsigned int joinPropExecutions;

  ////////////
  // Rename //
  ////////////
public:
  /// Number of rename constraints posted
  unsigned int renamePropPosts;
  /// Number of executions for the rename propagator
  unsigned int renamePropExecutions;

  /////////////////
  // Cardinality //
  /////////////////
public:
  /// Number of cardinality constraints posted
  unsigned int cardPropPosts;
  /// Number of executions for the cardinality propagator
  unsigned int cardPropExecutions;

  //////////////
  // Equality //
  //////////////
public:
  /// Number of equality constraints posted
  unsigned int eqPropPosts;
  /// Number of executions for the equality propagator
  unsigned int eqPropExecutions;

  //////////////////
  // Intersection //
  //////////////////
public:
  /// Number of intersection constraints posted
  unsigned int interPropPosts;
  /// Number of executions for the intersection propagator
  unsigned int interPropExecutions;

public:
  /// Default constructor
  CPRelStats()
      : projPropPosts(0)
      , projPropPostsFastUnique(0)
      , projPropExecutions(0)
      , joinPropPosts(0)
      , joinPropExecutions(0)
      , renamePropPosts(0)
      , renamePropExecutions(0)
      , cardPropPosts(0)
      , cardPropExecutions(0)
      , eqPropPosts(0)
      , eqPropExecutions(0)
      , interPropPosts(0)
      , interPropExecutions(0) {}
};
/**
 * @brief Function to access the statistics.
 */
CPRelStats& getCPRelStats(void);
}
}
