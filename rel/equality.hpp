/*
 *  Authors:
 *    Gustavo Gutierrez-Sabogal <gustavo.ggutierrez@gmail.com>
 *
 *  Copyright:
 *    Gustavo Gutierrez, 2013
 *
 *  Permission is hereby granted, free of charge, to any person obtaining
 *  a copy of this software, to deal in the software without restriction,
 *  including without limitation the rights to use, copy, modify, merge,
 *  publish, distribute, sublicense, and/or sell copies of the software,
 *  and to permit persons to whom the software is furnished to do so, subject
 *  to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 *  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 *  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#include "rel.hh"
#include <vector>
#include <algorithm>

using Gecode::ExecStatus;
using Gecode::PropCost;
using Gecode::ModEventDelta;
using Gecode::ES_OK;
using Gecode::ES_NOFIX;
using Gecode::ES_FIX;
using Gecode::ES_FAILED;

using Gecode::Set::PC_SET_ANY;
using Gecode::Iter::Values::Array;
using Gecode::Iter::Values::ToRanges;
using Gecode::Iter::Ranges::ToValues;
using Gecode::Set::GlbRanges;
using Gecode::Set::LubRanges;

using CuddAbstraction::Schema;

namespace MPG {
namespace Rel {

template <class View0, class View1>
class Equality : public Propagator {
protected:
  View0 x;
  View1 y;

public:
  /////////////
  // posting //
  /////////////
  Equality(Space& home, View0 x0, View1 x1)
      : Propagator(home)
      , x(x0)
      , y(x1) {
    assert(x.glb().schema() == y.glb().schema());
    x.subscribe(home, *this, PC_REL_ANY);
    y.subscribe(home, *this, PC_REL_ANY);
  }
  static ExecStatus post(Space& home, View0 x0, View1 y0) {
    // TODO: throw exception if x0 has not the same domain as y0
    (void)new (home) Equality(home, x0, y0);
    getCPRelStats().eqPropPosts++;
    return ES_OK;
  }
  //////////////
  // disposal //
  //////////////
  virtual size_t dispose(Space& home) {
    x.cancel(home, *this, PC_REL_ANY);
    y.cancel(home, *this, PC_REL_ANY);
    (void)Propagator::dispose(home);
    return sizeof(*this);
  }
  /////////////
  // copying //
  /////////////
  Equality(Space& home, bool share, Equality& p)
      : Propagator(home, share, p) {
    x.update(home, share, p.x);
    y.update(home, share, p.y);
  }
  virtual Propagator* copy(Space& home, bool share) {
    return new (home) Equality(home, share, *this);
  }
  //////////////////////
  // cost computation //
  //////////////////////
  virtual PropCost cost(const Space&, const ModEventDelta&) const {
    return PropCost::binary(PropCost::LO);
  }
  /////////////////
  // propagation //
  /////////////////
  virtual ExecStatus propagate(Space& home, const ModEventDelta&) {
    LOG_OPERATION_NARGS("Prop::eq");
    getCPRelStats().eqPropExecutions++;

    Relation r = x.lub().intersectWith(y.lub());
    {
      GECODE_ME_CHECK(x.include(home, y.glb()));
      GECODE_ME_CHECK(x.exclude(home, r.complement()));
    }

    {
      GECODE_ME_CHECK(y.include(home, x.glb()));
      GECODE_ME_CHECK(y.exclude(home, r.complement()));
    }

    if (x.assigned()) {
      assert(y.assigned());
      return home.ES_SUBSUMED(*this);
    }
    return shared(x, y) ? ES_NOFIX : ES_FIX;
  }
};

/**
 * @brief Channel constraint between a set variable and a unary relation
 */
template <class RView, class SView>
class SetEquality : public Propagator {
protected:
  RView x; ///< Relation variable
  SView y; ///< Set variable
public:
  /////////////
  // posting //
  /////////////
  SetEquality(Space& home, RView x0, SView x1)
      : Propagator(home)
      , x(x0)
      , y(x1) {
    // TODO: appropriate subscriptions
    x.subscribe(home, *this, PC_REL_ANY);
    y.subscribe(home, *this, PC_SET_ANY);
  }
  static ExecStatus post(Space& home, RView x0, SView y0) {
    // TODO: Adjust the values in the set and in the relation view in such a
    // way that they start with the smallest possible domains. For instance,
    // the maximum of the set has to be at most the maximum of the relation.
    // The minimum of the set has to be 0. The same is possible in the other
    // way, if the relation can represent more than the set then the tuples
    // need to be pruned.
    (void)new (home) SetEquality(home, x0, y0);
    return ES_OK;
  }
  //////////////
  // disposal //
  //////////////
  virtual size_t dispose(Space& home) {
    // TODO: appropriate disposal
    x.cancel(home, *this, PC_REL_ANY);
    y.cancel(home, *this, PC_SET_ANY);
    (void)Propagator::dispose(home);
    return sizeof(*this);
  }
  /////////////
  // copying //
  /////////////
  SetEquality(Space& home, bool share, SetEquality& p)
      : Propagator(home, share, p) {
    x.update(home, share, p.x);
    y.update(home, share, p.y);
  }
  virtual Propagator* copy(Space& home, bool share) {
    return new (home) SetEquality(home, share, *this);
  }
  //////////////////////
  // cost computation //
  //////////////////////
  virtual PropCost cost(const Space&, const ModEventDelta&) const {
    // The cost of this propagator is actually high because set and relation
    // variables use different representation. The bdd representing the
    // relation variable has to be traversed minterm-wise then sorted into a
    // range and the compared to the set variable.
    return PropCost::binary(PropCost::HI);
  }
  /////////////////
  // propagation //
  /////////////////
  /**
   * @brief Extracts the elements of @a bound into a vector of integers
   */
  std::vector<int> relationAsSet(const Relation& bound) const {
    // TODO: This operation is potentially slow, having a cache could amortize
    // the complexity.
    auto& relHome = getRelationSpace();
    std::vector<NumericTuple> tuples = bound.asTuples(relHome);

    std::vector<int> ret;
    ret.reserve(bound.cardinality());

    for (const NumericTuple& t : tuples)
      ret.push_back(t.at(0));

    std::sort(ret.begin(), ret.end());
    return ret;
  }
  /**
   * @brief Extracts the elements of @a bound into a relation value
   */
  template <typename ValIt>
  Relation setAsRelation(ValIt bound) const {
    // TODO: This operation is potentially slow, having a cache could amortize
    // the complexity.
    auto& relHome = getRelationSpace();
    Relation ret(relHome, x.glb().schema());
    while (bound()) {
      ret.add(relHome, bound.val());
      ++bound;
    }
    return ret;
  }
  virtual ExecStatus propagate(Space& home, const ModEventDelta&) {
    // Include the lower bound of the relation in the set
    {
      std::vector<int> lb = relationAsSet(x.glb());
      Array lba(lb.data(), lb.size());
      ToRanges<Array> lbar(lba);
      GECODE_ME_CHECK(y.includeI(home, lbar));
    }

    // Intersect the upper bound of the relation with the set
    {
      std::vector<int> ub = relationAsSet(x.lub());
      Array uba(ub.data(), ub.size());
      ToRanges<Array> ubar(uba);
      GECODE_ME_CHECK(y.intersectI(home, ubar));
    }
    // Include the lower bound of the set in the relation
    {
      GlbRanges<SView> glb(y);
      ToValues<decltype(glb)> glbv(glb);
      Relation lb = setAsRelation(glbv);
      GECODE_ME_CHECK(x.include(home, lb));
    }
    // Exclude the complement of the upper bound of the set from the relation
    {
      LubRanges<SView> lub(y);
      ToValues<decltype(lub)> lubv(lub);
      Relation ub = setAsRelation(lubv);
      GECODE_ME_CHECK(x.exclude(home, ub.complement()));
    }
    // TODO: Check fix point reasoning
    if (x.assigned() && y.assigned())
      return home.ES_SUBSUMED(*this);

    return ES_FIX;
  }
};
}
}
