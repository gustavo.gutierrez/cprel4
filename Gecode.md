# Gecode configuration

When building gecode with support for relation variables it is important to take
into account:

  - The definition -DGECODE_HAS_REL_VARS must be present. If this is not the
    case then the compilation of gecode will fail because the relation variables
    are only build when the definition is present

As an example, the following configuration will build gecode with support for
relation decision variables.

```sh
./configure CC=cc CXX=c++ CXXFLAGS="-DGECODE_HAS_REL_VARS"  --with-vis=../newvar4/int.vis --enable-debug --disable-driver --disable-flatzinc --disable-examples --disable-minimodel --disable-gist --disable-gcc-visibility --prefix=/tmp/usr
```