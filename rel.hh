/*
 *  Authors:
 *    Gustavo Gutierrez-Sabogal <gustavo.ggutierrez@gmail.com>
 *
 *  Copyright:
 *    Gustavo Gutierrez, 2013
 *
 *  Permission is hereby granted, free of charge, to any person obtaining
 *  a copy of this software, to deal in the software without restriction,
 *  including without limitation the rights to use, copy, modify, merge,
 *  publish, distribute, sublicense, and/or sell copies of the software,
 *  and to permit persons to whom the software is furnished to do so, subject
 *  to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 *  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 *  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#ifndef __MPG_REL_HH__
#define __MPG_REL_HH__
#include <climits>
#include <iostream>
#include <sstream>

// The following code is required to mute a warning caused in
// array.hpp:1576 where a nullptr is returned in a new operator.
// the current version of clang complains about that.
// #pragma clang diagnostic push
// #pragma clang diagnostic ignored "-Wnew-returns-null"
#include "gecode/kernel.hh"
#include "gecode/set.hh"
#include "gecode/int.hh"
// #pragma clang diagnostic pop
#include "bdd-abstraction.hh"

using Gecode::Advisor;
using Gecode::Delta;
using Gecode::Exception;
using Gecode::Home;
using Gecode::IntVar;
using Gecode::ModEvent;
using Gecode::Propagator;
using Gecode::PropCond;
using Gecode::SetVar;
using Gecode::Space;
using Gecode::VarArgArray;
using Gecode::VarArray;
using Gecode::VarImpVar;

using CuddAbstraction::Relation;
using CuddAbstraction::Schema;
using CuddAbstraction::SpaceManager;

/**
 * Relation values are stored by BDDs.
 *
 * For efficiency reasons, BDDs need to be stored in a common place and most of
 * the operations on them use the concept of a BDD space or manager.
 */
namespace MPG {
namespace Rel {
/**
 * @brief Relation space initialization
 *
 * @param statsOnDestruction: Whether to print BDD usage statistics on manager
 *     destruction.
 * @param slotsInUniqueTable: Number of initial slots in the unique table. Must
 *     be a power of two.
 * @param  slotsInCache: Number of slots in the cache. Must be a power of two.
 * @param  maxMemory Maximum memory (i.e. 256 * MemSize::MB) to use a maximum of
 *     256 Mega bytes. Use 0 for CUDD default.
 * @param cacheMinEff: Minimum efficiency for cache resizing (use 0 for CUDD
 *     default)
 * @param maxSlotsInCache: hard limit for the maximum number of entries in the
 *     cache
 * @param dynamicReorder: Whether to enable or not dynamic reordering. In case
 *     of being true the reordering method is set to CUDD_REORDER_SYMM_SIFT
 */
void initializeRelationSpace(
    bool statsOnDestruction = false,
    unsigned int slotsInUniqueTable = CUDD_UNIQUE_SLOTS,
    unsigned int slotsInCache = CUDD_CACHE_SLOTS, unsigned long maxMemory = 0,
    unsigned int cacheMinEff = 0, unsigned int maxSlotsInCache = 0,
    bool dynamicReorder = false);
/**
 * @brief Size definitions to improve code clarity when using quantities.
 */
enum MemSize {
  KB = 1024ul,
  MB = 1024ul * 1024ul,
  GB = 1024ul * 1024ul * 1024ul
};
/**
 * @brief Returns a reference to the BDD manager used to store all the BDD
 *     representations of relations.
 */
SpaceManager& getRelationSpace(void);
}
}

// Statistics
#include "rel/stats.hpp"

// Operations on ground values
#include "rel/relop.hpp"

// exceptions
namespace MPG {
namespace Rel {
class OutOfLimits : public Exception {
public:
  OutOfLimits(const char* l)
      : Exception(l, "Number out of limits") {}
};

class VariableEmptyDomain : public Exception {
public:
  VariableEmptyDomain(const char* l)
      : Exception(l, "Attempt to create variable with empty domain") {}
};

class SetIncompatible : public Exception {
public:
  SetIncompatible(const char* l)
      : Exception(l, "Attempt to operate set incompatible relations") {}
};

class JoinIncompatible : public Exception {
public:
  JoinIncompatible(const char* l)
      : Exception(l, "Attempt to operate join incompatible relations") {}
};

class ComposeIncompatible : public Exception {
public:
  ComposeIncompatible(const char* l)
      : Exception(l, "Attempt to operate compose incompatible relations") {}
};

class ProjectionIncompatible : public Exception {
public:
  ProjectionIncompatible(const char* l)
      : Exception(l, "Attempt to operate projection incompatible relations") {}
};
}
}

// variable implementation
namespace MPG {
namespace Rel {

// limits
namespace Limits {
const int max = (INT_MAX / 2) - 1;
const int min = -max;
}
// delta for advisors
class RelDelta : public Delta {
private:
  bool glbChanged_;
  bool lubChanged_;

public:
  RelDelta(const Relation& includedInGlb, const Relation& excludedFromLub)
      : glbChanged_(!includedInGlb.isEmpty())
      , lubChanged_(!excludedFromLub.isEmpty()) {}
  bool glbChanged(void) const { return glbChanged_; }
  bool lubChanged(void) const { return lubChanged_; }
};

class RelVarImp : public RelVarImpBase {
protected:
  /// Interval lower bound
  Relation glb_;
  /// Interval upper bound
  Relation lub_;

public:
  /**
   * @brief Constructs a variable implementation with @a glb as the relation for
   *     the greatest lower bound and @a lub as the relation for the least upper
   *     bound.
   */
  RelVarImp(Space& home, const Relation& glb, const Relation& lub);
  /**
   * @brief Constructs a variable implementation with an empty relation as lower
   *     bound and a full relation as upper bound. Both relations are on schema
   *     @a sch
   */
  RelVarImp(Space& home, const Schema& sch);
  /**
   * @brief Disposal of relation variable implementations.
   */
  void dispose(Space& home) {
    glb_.~Relation();
    lub_.~Relation();
  }

public:
  // Access operations
  const Relation& glb(void) const { return glb_; }
  const Relation& lub(void) const { return lub_; }
  const Schema& schema(void) const { return glb_.schema(); }

public:
  // Assignment test
  bool assigned(void) const { return glb_ == lub_; }

public:
  // Modification operations
  ModEvent include(Space& home, const Relation& r) {
    assert(schema().sameAs(r.schema()));
    if (r.isSubsetOf(glb_))
      return ME_REL_NONE;
    if (!r.isSubsetOf(lub_))
      return ME_REL_FAILED;
    glb_ = glb_.unionWith(r);

    Relation empty(getRelationSpace(), schema());
    RelDelta d(r, empty);

    return notify(home, assigned() ? ME_REL_VAL : ME_REL_GLB, d);
  }
  ModEvent exclude(Space& home, const Relation& r) {
    assert(schema().sameAs(r.schema()));
    if (!r.isDisjointWith(glb_))
      return ME_REL_FAILED;
    if (r.isDisjointWith(lub_))
      return ME_REL_NONE;
    lub_ = lub_.differenceWith(r);

    Relation empty(getRelationSpace(), schema());
    RelDelta d(empty, r);

    return notify(home, assigned() ? ME_REL_VAL : ME_REL_LUB, d);
  }

public:
  // Subscriptions
  void subscribe(Space& home, Propagator& p, PropCond pc,
                 bool schedule = true) {
    RelVarImpBase::subscribe(home, p, pc, assigned(), schedule);
  }
  void subscribe(Space& home, Advisor& a) {
    RelVarImpBase::subscribe(home, a, assigned());
  }
  void cancel(Space& home, Propagator& p, PropCond pc) {
    RelVarImpBase::cancel(home, p, pc, assigned());
  }
  void cancel(Space& home, Advisor& a) {
    RelVarImpBase::cancel(home, a, assigned());
  }

public:
  // Copying
  RelVarImp(Space& home, bool share, RelVarImp& y)
      : RelVarImpBase(home, share, y)
      , glb_(y.glb_)
      , lub_(y.lub_) {}

  RelVarImp* copy(Space& home, bool share) {
    if (copied())
      return static_cast<RelVarImp*>(forward());
    else
      return new (home) RelVarImp(home, share, *this);
  }

public:
  // Delta information
  static bool glbChanged(const Delta& d) {
    return static_cast<const RelDelta&>(d).glbChanged();
  }
  static bool lubChanged(const Delta& d) {
    return static_cast<const RelDelta&>(d).lubChanged();
  }
};

inline bool subsumesME(ModEvent me0, ModEvent me1, ModEvent me) {
  ModEvent cme = RelVarImp::me_combine(me0, me1);
  return RelVarImp::me_combine(cme, me) == cme;
}

inline bool subsumesME(ModEvent me0, ModEvent me) {
  return RelVarImp::me_combine(me0, me) == me0;
}

inline bool testRelEventLB(ModEvent me0, ModEvent me1) {
  return subsumesME(me0, me1, ME_REL_GLB);
}

inline bool testRelEventUB(ModEvent me0, ModEvent me1) {
  return subsumesME(me0, me1, ME_REL_LUB);
}

inline bool testRelEventLB(ModEvent me0) { return subsumesME(me0, ME_REL_GLB); }

inline bool testRelEventUB(ModEvent me0) { return subsumesME(me0, ME_REL_LUB); }
}
}

// variable
namespace MPG {

class RelVar : public VarImpVar<Rel::RelVarImp> {
protected:
  using VarImpVar<Rel::RelVarImp>::x;

public:
  RelVar(void) {}
  RelVar(const RelVar& y)
      : VarImpVar<Rel::RelVarImp>(y.varimp()) {}
  RelVar(Rel::RelVarImp* y)
      : VarImpVar<Rel::RelVarImp>(y) {}

public:
  /**
   * @brief Creates a relation decision variable with @a glb as the relation for
   *     the greatest lower bound and @a lub as the relation for the least upper
   *     bound.
   */
  RelVar(Space& home, const Relation& glb, const Relation& lub)
      : VarImpVar<Rel::RelVarImp>(new (home) Rel::RelVarImp(home, glb, lub)) {
    // TODO: Throw an exception if a variable is created on a wrong schema
    /*
    if ((min < Rel::Limits::min) || (max > Rel::Limits::max))
      throw Rel::OutOfLimits("RelVar::RelVar");
    if (min > max)
      throw Rel::VariableEmptyDomain("RelVar::RelVar");
    */
  }
  RelVar(Space& home, const Schema& sch)
      : VarImpVar<Rel::RelVarImp>(new (home) Rel::RelVarImp(home, sch)) {}

public:
  // access operations
  /**
   * @brief Returns the greatest lower bound associated to this variable.
   */
  const Relation& glb(void) const { return x->glb(); }
  /**
   * @brief Returns the least upper bound associated to this variable.
   */
  const Relation& lub(void) const { return x->lub(); }
  /**
   * @brief Returns the schema of the relations represented by this variable.
   */
  const Schema& schema(void) const { return x->glb().schema(); }
};
}

// array traits
namespace MPG {
class RelVarArgs;
class RelVarArray;
}

namespace Gecode {

template <>
class ArrayTraits<Gecode::VarArray<MPG::RelVar>> {
public:
  typedef MPG::RelVarArray StorageType;
  typedef MPG::RelVar ValueType;
  typedef MPG::RelVarArgs ArgsType;
};
template <>
class ArrayTraits<MPG::RelVarArray> {
public:
  typedef MPG::RelVarArray StorageType;
  typedef MPG::RelVar ValueType;
  typedef MPG::RelVarArgs ArgsType;
};
template <>
class ArrayTraits<Gecode::VarArgArray<MPG::RelVar>> {
public:
  typedef MPG::RelVarArgs StorageType;
  typedef MPG::RelVar ValueType;
  typedef MPG::RelVarArgs ArgsType;
};
template <>
class ArrayTraits<MPG::RelVarArgs> {
public:
  typedef MPG::RelVarArgs StorageType;
  typedef MPG::RelVar ValueType;
  typedef MPG::RelVarArgs ArgsType;
};
}
// variable arrays
namespace MPG {

class RelVarArgs : public VarArgArray<RelVar> {
public:
  RelVarArgs(void) {}
  explicit RelVarArgs(int n)
      : VarArgArray<RelVar>(n) {}
  RelVarArgs(const RelVarArgs& a)
      : VarArgArray<RelVar>(a) {}
  RelVarArgs(const VarArray<RelVar>& a)
      : VarArgArray<RelVar>(a) {}
  RelVarArgs(Space& home, int n, const Relation& glb, const Relation& lub)
      : VarArgArray<RelVar>(n) {
    for (int i = 0; i < n; i++)
      (*this)[i] = RelVar(home, glb, lub);
  }
};

class RelVarArray : public VarArray<RelVar> {
public:
  RelVarArray(void) {}
  RelVarArray(const RelVarArray& a)
      : VarArray<RelVar>(a) {}
  RelVarArray(Space& home, int n, const Relation& glb, const Relation& lub)
      : VarArray<RelVar>(home, n) {
    for (int i = 0; i < n; i++)
      (*this)[i] = RelVar(home, glb, lub);
  }
};
}

// Printing of relations
namespace MPG {
/**
 *  @brief Storage for information on whether to print relations in HTM or
 *      plain text.
 *
 *  By default we use 0 to represent normal text output and 1 to represent
 *  Html output.
 */
static int const HtmlPrinting = std::ios_base::xalloc();
/**
 * @brief Returns if HTML format should be used to print relations
 */
inline bool useHtmlForOutput(std::ostream& os) {
  return 0 != os.iword(HtmlPrinting);
}
/**
 * @brief Sets the stream @a os to use Html printing for relations
 */
inline std::ostream& HtmlOutput(std::ostream& os) {
  os.iword(HtmlPrinting) = 1;
  return os;
}
/**
 * @brief Sets the stream @a os to use text printing for relations
 */
inline std::ostream& TextOutput(std::ostream& os) {
  os.iword(HtmlPrinting) = 0;
  return os;
}
}

#include "rel/view.hpp"

namespace MPG {
template <class Char, class Traits>
std::basic_ostream<Char, Traits>&
operator<<(std::basic_ostream<Char, Traits>& os, const RelVar& x) {
  std::basic_ostringstream<Char, Traits> s;
  s.copyfmt(os);
  s.width(0);
  Rel::RelView vx(x);
  os << vx;
  return os << s.str();
}

template <class Char, class Traits>
void print(std::basic_ostream<Char, Traits>& os,
           CuddAbstraction::DomainGroup& dg, const RelVar& x) {
  std::basic_ostringstream<Char, Traits> s;
  s.copyfmt(os);
  s.width(0);
  Rel::RelView vx(x);
  print(os, dg, vx);
}
}

#include "rel/branch.hpp"

#ifdef GECODE_HAS_GIST
#include "rel/inspector.hpp"
#endif

////////////////////////////////
// Propagator implementations //
////////////////////////////////
#include "rel/equality.hpp"
#include "rel/intersection.hpp"
#include "rel/cardinality.hpp"
#include "rel/projection.hpp"
#include "rel/join.hpp"
#include "rel/rename.hpp"

namespace MPG {
/**
 * Posting function for the constraint
 * x = y
 */
void equal(Space& home, RelVar x, RelVar y);
/**
 * Posting function for the constraint
 * x = y
 *
 * @warn @a x must be a unary relation
 */
void equal(Space& home, RelVar x, SetVar y);
/**
 * Posting function for the constraint
 * y = \complement{x}
 */
void complement(Space& home, RelVar x, RelVar y);
/**
 * Posting function for the constraint
 * z = x \intersect y
 */
void intersection(Space& home, RelVar x, RelVar y, RelVar z);
/**
 * Posting function for the constraint
 * z = z \union y
 */
void Union(Space& home, RelVar x, RelVar y, RelVar z);
/**
 * Posting function for the constraint
 * x \intersect y = \emptyset
 */
void disjoint(Space& home, RelVar x, RelVar y);
/**
 * Posting function for the constraint
 * x \subseteq y
 */
void subset(Space& home, RelVar x, RelVar y);
/**
 * Posting function for the constraint
 * y = \project{a}{x}
 */
void projection(Space& home, RelVar x, const Schema& a, RelVar y);
/**
 * Posting function for the constraint
 * z = x \join_a y
 */
void join(Space& home, RelVar x, RelVar y, RelVar z);
/**
 * Posting function for the constraint
 * z = x \compose_a y
 *
 * The constraint is enforced by two constraints:
 * z' = \join{x}{y}
 * z = \proj{\attributes{z}}{z'}
 *
 * Where z' is a temporary variable on schema x.schema() \cup y.schema{}
 */
void compose(Space& home, RelVar x, RelVar y, RelVar z);
/**
 * Posting function for the constraint
 * y =_{m} x
 */
void rename(Space& home, RelVar x, const AttributeMap& m, RelVar y);
/**
 * Posting function for the constraint
 * y = |x|
 */
void cardinality(Space& home, RelVar x, IntVar y);
}

#endif