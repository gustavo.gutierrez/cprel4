#!/bin/sh

CLANG_FORMAT=/Users/gg/local/bin/clang-format

find . -type f -not -path "*gtest/*" \( -iname "*.cc" -or -iname "*.cpp" -or -iname "*.hh" -or -iname "*.hpp" -or -iname "*.c" -or -iname "*.h" \) -print0 | while IFS= read -r -d $'\0' line; do
    echo "$line"
    $CLANG_FORMAT -i "$line"
    # ls -l "$line"    
done
