#ifndef __STRING_HANDLER_DECL_HH__
#define __STRING_HANDLER_DECL_HH__

#include "integer-datum-decl.hh"
#include "relation-decl.hh"
#include "agg-relation-decl.hh"

#include <unordered_map>

// The following code tell clang to behave silent with the specific warning.
// This is a problem of boost as of today:
// https://svn.boost.org/trac/boost/ticket/8743
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wredeclared-class-member"
#include <boost/bimap.hpp>
#pragma clang diagnostic pop

namespace CuddAbstraction {

namespace bb = boost::bimaps;

/**
 * @brief Exception thrown when an attribute cannot be encoded as a domain
 *     element.
 */
class InsufficientDomainCardinality : public Exception {
public:
  InsufficientDomainCardinality(void) = delete;
  InsufficientDomainCardinality(const char* location)
      : Exception(location, "Attempt use a domain with not enough cardinality "
                            "in an operation") {}
};
/**
 * @brief Exception thrown when an invalid encoding is used.
 */
class InvalidEncoding : public Exception {
public:
  InvalidEncoding(void) = delete;
  InvalidEncoding(const char* location)
      : Exception(location, "Attempt use an invalid encoding") {}
};

/**
 * @brief A domain group is an object used to keep consistent encoding of
 *     strings to integers across relations.
 */
class DomainGroup {
  // Type definitions for a single encoding <-> value mapping
private:
  /// Left tag for the bimap: encoding
  struct encoding {};
  /// Right tag for the bimap: value
  struct value {};
  /// Storage type for the correspondence between encodings to values
  typedef bb::bimap<bb::tagged<std::string, value>,
                    bb::tagged<IntegerDatum, encoding>> DomainMapping;
  /// Type definition for entries in a DomainMapping
  typedef DomainMapping::value_type Entry;

private:
  /// Actual storage for mappings
  typedef std::unordered_map<AttributeDomain, DomainMapping> Mappings;
  Mappings map_;
  /// Storage for the maximum width of every domain
  std::unordered_map<AttributeDomain, size_t> widths_;

public:
  /// Creates an empty group of domains
  DomainGroup(void);
  /// Adds a new domain to the group
  void add(AttributeDomain d);
  /// Adds the domain of attribute @a a to the group
  void add(const Attribute& a);
  /// Returns the mapping associated to attribute @a att
  DomainMapping& getMapping(const Attribute& att);
  /**
   * @brief Tests whether there is a mapping for the domain of @a att in this
   *    group.
   */
  bool isThereMappingFor(const Attribute& att) const;
  /// Get an encoding for a string @a str in the domain of attribute @a att
  IntegerDatum getEncoding(const std::string& str, const Attribute& att);
  /**
   * @brief Returns the string associated to encoding @a enc in the domain of
   *     attribute @a att
   */
  const std::string& getValueEncodedBy(const IntegerDatum& enc,
                                       const Attribute& att);
  /**
   * @brief Tests whether this group has entries for the attributes in @a sch
   */
  bool hasEntriesFor(const Schema& sch) const;
  /// Returns the width of the domain of attribute @a att
  size_t getWidth(const Attribute& att) const;
};
/**
 * @brief Operator overloading for domain groups and attributes
 *
 * This operation adds attribute @a a to the domain group.
 */
DomainGroup& operator<<(DomainGroup& dg, const Attribute& a);
/**
 * @brief Adds the tuple described by @a Args to the relation @a rel.
 *
 * Every element of the tuple must be a string and are inserted in the relation
 * according to the order of the attributes in which its domain was defined.
 * There must be an entry in @a dg for every domain of each attribute of @a rel
 * in @a dg.
 */
template <typename... Args>
void insert(const SpaceManager& home, DomainGroup& dg, Relation& rel, Args...);
/**
 * @brief Inserts the tuple described by the range [@start, @end) into the
 *  relation @a rel.
 *
 * @warning The type of the elements described by the iterator must be strings.
 */
template <typename It>
void insertIT(const SpaceManager& home, DomainGroup& dg, Relation& rel,
              It start, It end);
/**
 * @brief Loads the relation specified in @a input into @a rel.
 *
 * - Every line in the file is a tuple
 * - Tuple elements are strings separated by spaces
 */
void readFromStream(const SpaceManager& home, DomainGroup& dg, Relation& rel,
                    std::istream& input);

/**
 * @brief Inserts into aggregate relation
 */
template <typename... Args>
void insert(const SpaceManager& home, DomainGroup& dg, AggRelation& rel,
            int agg, Args...);
}
#endif