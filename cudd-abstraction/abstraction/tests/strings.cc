#include <iostream>
#include <gtest/gtest.h>

#include "bdd-abstraction.hh"

using namespace std;
using namespace CuddAbstraction;

TEST(StringHandler, attributeEncoding) {
  SpaceManager home;
  {
    Attribute c0(home, makeDomain(4)), c1(home, makeDomain(8));
    DomainGroup dg;
    dg.add(c0);
    dg.add(c1);

    Attribute c2(home, makeDomain(4), "C2");
    EXPECT_TRUE(dg.isThereMappingFor(c0));
    EXPECT_TRUE(dg.isThereMappingFor(c1));
    EXPECT_FALSE(dg.isThereMappingFor(c2));

    EXPECT_EQ(dg.getEncoding("zoe", c0), 0);
    EXPECT_EQ(dg.getEncoding("carl", c0), 1);
    EXPECT_EQ(dg.getEncoding("bob", c0), 2);
    EXPECT_EQ(dg.getEncoding("sponge", c0), 3);

    // an extra element does not fit the cardinality of the domain
    EXPECT_THROW({ dg.getEncoding("bart", c0); },
                 InsufficientDomainCardinality);

    EXPECT_EQ(dg.getValueEncodedBy(0, c0), "zoe");
    EXPECT_EQ(dg.getValueEncodedBy(1, c0), "carl");
    EXPECT_EQ(dg.getValueEncodedBy(2, c0), "bob");
    EXPECT_EQ(dg.getValueEncodedBy(3, c0), "sponge");

    // ask the encoding of an inexistant value raises an exception
    EXPECT_THROW({ dg.getValueEncodedBy(5, c0); }, InvalidEncoding);
  }
  EXPECT_EQ(0, home.zeroReferences());
}

TEST(StringHandler, relationCreation) {
  SpaceManager home;
  {
    Attribute c0(home, makeDomain(4), "Character1"),
        c1(home, makeDomain(8), "Character2");
    DomainGroup dg;
    dg.add(c0);
    dg.add(c1);
    Schema domR(home, {c0, c1});
    Relation r(home, domR);

    // Adds the tuple <c0:bart c1:lisa> to relation r
    insert(home, dg, r, "bart", "lisa");
    EXPECT_EQ(r.cardinality(), 1);
    EXPECT_EQ(dg.getEncoding("bart", c0), 0);
    EXPECT_EQ(dg.getEncoding("lisa", c1), 0);

    // Adds the tuple <c0:bart c1:moe> to relation r
    insert(home, dg, r, "bart", "moe");
    EXPECT_EQ(r.cardinality(), 2);
    EXPECT_EQ(dg.getEncoding("bart", c0), 0);
    EXPECT_EQ(dg.getEncoding("moe", c1), 1);

    // Adds the tuple <c0:lisa c1:moe> to relation r
    insert(home, dg, r, "lisa", "moe");
    EXPECT_EQ(r.cardinality(), 3);
    EXPECT_EQ(dg.getEncoding("lisa", c0), 1);
    EXPECT_EQ(dg.getEncoding("moe", c1), 1);

    Attribute sender(home, makeDomain(3), "S");
    dg.add(sender);
    Schema sch0(home, {c0, sender});
    Relation s(home, sch0);

    insert(home, dg, s, "bart", "one");
    EXPECT_EQ(s.cardinality(), 1);
    EXPECT_EQ(dg.getEncoding("one", sender), 0);
    EXPECT_EQ(dg.getEncoding("bart", c0), 0);

    // Try a ternary relation
    Schema sch1(home, {c0, c1, sender});
    Relation q(home, sch1);
    insert(home, dg, q, "maggie", "lisa", "two");
    insert(home, dg, q, "maggie", "homer", "two");
    insert(home, dg, q, "maggie", "marge", "one");
    EXPECT_EQ(q.cardinality(), 3);

    // Test printing with domain group
    Formatter f(q, std::cout, &dg);
    f.print(home);
  }
  EXPECT_EQ(0, home.zeroReferences());
}

TEST(StringHandler, longRelationPrint) {
  SpaceManager home;
  {
    Attribute c0(home, makeDomain(4), "Character1"),
        c1(home, makeDomain(8), "Character2");
    DomainGroup dg;
    dg.add(c0);
    dg.add(c1);
    Schema domR(home, {c0, c1});
    Relation r(home, domR);

    std::string x(50, 'a'), y(50, 'b');
    insert(home, dg, r, x, y);

    Formatter f(r, std::cout, &dg);
    f.print(home);
  }
  EXPECT_EQ(0, home.zeroReferences());
}
TEST(StringHandler, iterationInsertion) {
  SpaceManager home;
  {
    Attribute c0(home, makeDomain(8)), c1(home, makeDomain(8));
    DomainGroup dg;
    dg.add(c0);
    dg.add(c1);

    Schema domR(home, {c0, c1});
    Relation r(home, domR);

    std::vector<std::string> t0 = {"bart", "lisa"}, t1 = {"marge", "hommer"};
    insertIT(home, dg, r, t0.begin(), t0.end());
    EXPECT_EQ(r.cardinality(), 1u);
    insertIT(home, dg, r, t1.begin(), t1.end());
    EXPECT_EQ(r.cardinality(), 2u);
    insertIT(home, dg, r, t0.begin(), t0.end());
    EXPECT_EQ(r.cardinality(), 2u);

// Passing iterators that provide less elements than relation's arity does
// not have an effect. In debug mode however, attempting to do so rises an
// assertion.
#ifdef NDEBUG
    std::vector<std::string> e0 = {"bart"}, e1 = {"marge", "hommer", "morgan"};
    insertIT(home, dg, r, e0.begin(), e0.end());
    EXPECT_EQ(r.cardinality(), 2u);
    insertIT(home, dg, r, e1.begin(), e1.end());
    EXPECT_EQ(r.cardinality(), 2u);
#endif
    std::string someTuples = "moe hommer\nmaggie lisa\ncat dog";
    std::stringstream is(someTuples);
    readFromStream(home, dg, r, is);
    EXPECT_EQ(r.cardinality(), 5u);
  }
  EXPECT_EQ(0, home.zeroReferences());
}

TEST(StringHandler, aggregatedRelation) {
  SpaceManager home;
  {
    Attribute a(home, makeDomain(4), "A"), b(home, makeDomain(4), "B");
    DomainGroup dg;
    dg << a << b;

    Schema s(home, {a, b});
    Relation r(home, s);
    insert(home, dg, r, "foo", "bar");
    insert(home, dg, r, "foo", "joe");
    insert(home, dg, r, "zap", "bar");

    print(home, r, std::cout, &dg);

    AggRelation agg = AggRelation::groupBy(home, r, {a});
    Attribute c(home, makeDomain(16), "Agg");
    Relation k = agg.aggregatedAsAttribute(home,{c});
    print(home, k, std::cout, &dg);
  }
  EXPECT_EQ(0, home.zeroReferences());
}
