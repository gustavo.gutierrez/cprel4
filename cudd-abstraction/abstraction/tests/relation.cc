#include <iostream>
#include <gtest/gtest.h>

#include "bdd-abstraction.hh"

using namespace std;
using namespace CuddAbstraction;

TEST(RelationTest, binary_relation) {
  SpaceManager home;
  {
    Attribute c0(home, makeDomain(256)), c1(home, makeDomain(256));
    Schema domR(home, {c0, c1});
    Relation r(home, domR);

    EXPECT_EQ(0, r.cardinality());
    int max_i = 100, max_j = 100;
    for (int i = 0; i < max_i; i++) {
      for (int j = 0; j < max_j; j++) {
        r.add(home, {i, j});
      }
    }
    EXPECT_EQ(max_i * max_j, r.cardinality());
    // printRelation(home, r);

    for (int i = 0; i < max_i; i++)
      for (int j = 0; j < max_j; j++) {
        auto old_card = r.cardinality();
        r.remove(home, {i, j});
        EXPECT_EQ(old_card - 1, r.cardinality());
      }
    EXPECT_EQ(0, r.cardinality());
  }
  EXPECT_EQ(0, home.zeroReferences());
}

TEST(Relvalue, zeroArity) {
  SpaceManager home;
  {
    Attribute emptyDom(home, makeDomain(0));
    EXPECT_EQ(emptyDom.reprSize(), 0u);

    Schema domr(home, {});
    Relation r(home, domr);
    EXPECT_EQ(r.arity(), 0u);
    EXPECT_EQ(r.cardinality(), 0);
    // The only element that can exist in r is the empty tuple
    EXPECT_EQ(r.maxCardinality(), 1);
    r.add(home, {});
    EXPECT_EQ(r.cardinality(), 1);
    EXPECT_EQ(r.maxCardinality(), 1);
  }
  EXPECT_EQ(0, home.zeroReferences());
}

TEST(Relvalue, fullCreation) {
  SpaceManager home;
  {
    Attribute c0(home, makeDomain(2));
    Attribute c1(home, makeDomain(4));
    Attribute c2(home, makeDomain(8));

    Schema rc0(home, {c0});
    Relation fullRC0 = Relation::createFull(home, rc0);
    EXPECT_EQ(fullRC0.cardinality(), 2);
    EXPECT_TRUE(fullRC0.isFull());

    Schema rc0c1(home, {c0, c1});
    Relation fullRC0C1 = Relation::createFull(home, rc0c1);
    EXPECT_EQ(fullRC0C1.cardinality(), ((1 << 1) * (1 << 2)));
    EXPECT_TRUE(fullRC0C1.isFull());

    Schema rc0c1c2(home, {c0, c1, c2});
    Relation fullRC0C1C2 = Relation::createFull(home, rc0c1c2);
    EXPECT_EQ(fullRC0C1C2.cardinality(), ((1 << 1) * (1 << 2) * (1 << 3)));
    EXPECT_TRUE(fullRC0C1C2.isFull());
  }
  EXPECT_EQ(0, home.zeroReferences());
}

TEST(Relvalue, creation) {
  SpaceManager home;
  {
    Attribute c0(home, makeDomain(8));
    Attribute c1(home, makeDomain(8));
    Attribute c2(home, makeDomain(8));
    Attribute c3(home, makeDomain(8));
    Attribute c4(home, makeDomain(8));
    Attribute c5(home, makeDomain(8));

    Schema c0c1c2(home, {c0, c1, c2});
    Relation f(home, c0c1c2);
    f.add(home, {0, 0, 0});
    f.add(home, {0, 0, 1});
    f.add(home, {0, 2, 0});
    f.add(home, {7, 0, 0});
    EXPECT_EQ(f.cardinality(), 4);

    // copy constructor
    Relation copyOfF(f);
    EXPECT_EQ(copyOfF.cardinality(), f.cardinality());

    // removal of tuples
    copyOfF.remove(home, {0, 0, 0});
    EXPECT_EQ(copyOfF.cardinality(), f.cardinality() - 1);

    // union
    Relation u = f.unionWith(copyOfF);
    EXPECT_EQ(u.cardinality(), f.cardinality());
    Relation uprime = copyOfF.unionWith(f);
    EXPECT_EQ(u.cardinality(), uprime.cardinality());
    EXPECT_TRUE(u.sameAs(uprime));

    // intersection
    Relation i = f.intersectWith(copyOfF);
    EXPECT_TRUE(i.sameAs(copyOfF));

    // complement
    Relation fcompl = f.complement();
    EXPECT_EQ(fcompl.cardinality(), fcompl.maxCardinality() - f.cardinality());

    // Intersection with the complement is the empty relation

    // cout << "Max card: " << f.maxCardinality() << endl;
    // std::vector<NumericTuple> contents = f.asTuples(home);
    // for (const auto& i : contents) {
    //  cout << i << endl;
    // }
  }
  EXPECT_EQ(0, home.zeroReferences());
}

TEST(Relvalue, subset) {
  SpaceManager home;
  {
    Attribute a(home, makeDomain(4));
    Attribute b(home, makeDomain(4));

    Schema s(home, {a, b});
    Relation r(home, s);
    Relation q(home, s);

    EXPECT_TRUE(r.isSubsetOf(q));
    EXPECT_TRUE(q.isSubsetOf(r));

    r.add(home, 0, 0);
    EXPECT_TRUE(q.isSubsetOf(r));
    EXPECT_FALSE(r.isSubsetOf(q));

    r.add(home, 0, 2);
    q.add(home, 0, 0);
    EXPECT_TRUE(q.isSubsetOf(r));

    Relation f = Relation::createFull(home, s);
    EXPECT_TRUE(r.isSubsetOf(f));
    EXPECT_TRUE(q.isSubsetOf(f));

    Relation e = Relation::createEmpty(home, s);
    EXPECT_TRUE(e.isSubsetOf(r));
    EXPECT_TRUE(e.isSubsetOf(q));
    EXPECT_TRUE(e.isSubsetOf(f));
  }
  EXPECT_EQ(0, home.zeroReferences());
}

TEST(Relvalue, cross) {
  SpaceManager home;
  {
    Attribute c0(home, makeDomain(8));
    Attribute c1(home, makeDomain(16));

    Schema RC0(home, {c0}), RC1(home, {c1});
    Relation f = Relation::createFull(home, RC0);
    Relation g = Relation::createFull(home, RC1);

    Relation fCrossG = f.crossWith(home, g);
    EXPECT_EQ(fCrossG.cardinality(), ((1 << 3) * (1 << 4)));

    Schema RC0C1(home, {c0, c1});
    Relation fullRC0RC1 = Relation::createFull(home, RC0C1);

    EXPECT_EQ(fullRC0RC1, fCrossG);
    EXPECT_EQ(RC0C1, fCrossG.schema());

    Relation a = Relation::createEmpty(home, RC0);
    a.add(home, {1});
    a.add(home, {2});
    a.add(home, {3});
    a.add(home, {4});

    Relation b = a.crossWith(home, RC1);
    EXPECT_EQ(b.schema(), RC0C1);
    EXPECT_EQ(b.cardinality(), a.cardinality() * (1 << 4));

    Attribute c2(home, makeDomain(32));
    Schema RC2(home, {c2});
    Relation c = Relation::createEmpty(home, RC2);
    c.add(home, {8});
    c.add(home, {10});

    Relation d = a.crossWith(home, c);
    EXPECT_EQ(d.cardinality(), a.cardinality() * c.cardinality());
    EXPECT_EQ(d.arity(), a.arity() + c.arity());
  }
  EXPECT_EQ(0, home.zeroReferences());
}

TEST(Relvalue, withDomain) {
  SpaceManager home;
  {

    AttributeDomain domain0 = makeDomain(8);
    AttributeDomain domain1 = makeDomain(8);
    AttributeDomain domain2 = makeDomain(8);

    Attribute c0(home, domain0), c1(home, domain1), c2(home, domain2);
    Schema schC(home, {c0, c1, c2});

    Attribute d0(home, domain0), d1(home, domain1), d2(home, domain2);
    Schema schD(home, {d0, d1, d2});

    Relation c(home, schC);
    c.add(home, {0, 0, 0});
    c.add(home, {7, 6, 5});

    {
      // Mapping to produce a copy of c on schema schD
      AttributeMap c2domD({{c0, d0}, {c1, d1}, {c2, d2}});
      EXPECT_TRUE(c2domD.mapsAllAttributesIn(schC));

      Relation d = c.withSchema(home, c2domD, schD);
      EXPECT_FALSE(d.sameSchemaAs(c));
      {
        Relation expectedD(home, schD);
        expectedD.add(home, {0, 0, 0});
        expectedD.add(home, {7, 6, 5});
        EXPECT_TRUE(expectedD.sameAs(d));
      }

      EXPECT_EQ(d.arity(), c.arity());
      EXPECT_FALSE(d.schema() == c.schema());
    }
  }
  EXPECT_EQ(0, home.zeroReferences());
}

TEST(Relvalue, projectTo) {
  SpaceManager home;
  {
    Attribute c0(home, makeDomain(8)), c1(home, makeDomain(8)),
        c2(home, makeDomain(8)), c3(home, makeDomain(8)),
        c4(home, makeDomain(8)), c5(home, makeDomain(8));

    Schema fd(home, {c0, c1, c2});

    Relation f(home, fd);
    f.add(home, {0, 0, 0});
    f.add(home, {0, 0, 1});
    f.add(home, {0, 2, 0});
    f.add(home, {7, 0, 0});

    Schema fdc0(home, {c0});
    Relation g = f.projectTo(home, fdc0);
    EXPECT_EQ(g.arity(), 1u);
    EXPECT_EQ(g.cardinality(), 2);

    {
      Relation expectedG(home, g.schema());
      expectedG.add(home, {0});
      expectedG.add(home, {7});
      EXPECT_TRUE(expectedG.sameAs(g));
    }

    Schema fdc1(home, {c1});
    Relation h = f.projectTo(home, fdc1);
    EXPECT_EQ(h.arity(), 1u);
    EXPECT_EQ(h.cardinality(), 2);
    {
      Relation expectedH(home, fdc1);
      expectedH.add(home, {0});
      expectedH.add(home, {2});
      EXPECT_TRUE(expectedH.sameAs(h));
    }
    Schema fdc0c1(home, {c0, c1});
    Relation i = f.projectTo(home, fdc0c1);
    EXPECT_EQ(i.arity(), 2u);
    EXPECT_EQ(i.cardinality(), 3);
    {
      Relation expectedI(home, fdc0c1);
      expectedI.add(home, {0, 0});
      expectedI.add(home, {0, 2});
      expectedI.add(home, {7, 0});
      EXPECT_TRUE(expectedI.sameAs(i));
    }
    // Projecting on an empty domain
    Schema emptyDom(home, {});
    Relation j = f.projectTo(home, emptyDom);
    EXPECT_EQ(j.arity(), 0u);
    EXPECT_EQ(j.cardinality(), 1);
    // The explanation for the one element in the resulting relation j
    // is that the relation that was projected on contained some tuples
    // this will lead to the empty tuple being in the result. The empty
    // tuple is a member of the relation of arity 0.

    // TODO: since the removal of isIn this is commented out. Fix it.
    // EXPECT_TRUE(j.isIn(home,{}));

    Relation k = f.projectTo(home, fd);
    EXPECT_TRUE(k.sameAs(f));

    Schema pc3(home, {c3});
    EXPECT_THROW({ Relation l = f.projectTo(home, pc3); },
                 ExpectingContainedDomains);
  }
  EXPECT_EQ(0, home.zeroReferences());
}

TEST(Relvalue, picktuple) {
  SpaceManager home;
  {
    Attribute c0(home, makeDomain(8)), c1(home, makeDomain(8)),
        c2(home, makeDomain(8));

    Schema c0c1c2(home, {c0, c1, c2});
    Relation f(home, c0c1c2);
    f.add(home, {0, 0, 0});
    f.add(home, {0, 0, 1});
    f.add(home, {0, 2, 0});
    f.add(home, {7, 0, 0});
    EXPECT_EQ(f.cardinality(), 4);

    for (auto i = f.cardinality(); i > 0; i--) {
      auto card0 = f.cardinality();
      NumericTuple t = f.pickRandomTuple();
      f.remove(home, t);
      EXPECT_EQ(f.cardinality(), card0 - 1);
    }
  }
  EXPECT_EQ(0, home.zeroReferences());
}

TEST(Relvalue, pickUnarySubrelation) {
  SpaceManager home;
  {
    Attribute c0(home, makeDomain(8)), c1(home, makeDomain(8)),
        c2(home, makeDomain(8));

    Schema sch(home, {c0, c1, c2});
    Relation f(home, sch);
    f.add(home, {0, 0, 0});
    f.add(home, {0, 0, 1});
    f.add(home, {0, 2, 0});
    f.add(home, {7, 0, 0});
    EXPECT_EQ(f.cardinality(), 4);

    Relation g(f);
    Relation h(home, sch);

    while (!f.isEmpty()) {
      Relation u = f.pickAnyUnarySubrelation(home);
      EXPECT_TRUE(u.isSubsetOf(f));
      h = h.unionWith(u);
      f = f.differenceWith(u);
    }
    EXPECT_TRUE(h.sameAs(g));

    // for (auto i = f.cardinality(); i > 0; i--) {
    //   auto card0 = f.cardinality();
    //   NumericTuple t = f.pickRandomTuple();
    //   f.remove(home, t);
    //   EXPECT_EQ(f.cardinality(), card0 - 1);
    // }
  }
  EXPECT_EQ(0, home.zeroReferences());
}

TEST(Relvalue, pickRandomSubrelation) {
  SpaceManager home;
  {
    Attribute c0(home, makeDomain(8)), c1(home, makeDomain(8)),
        c2(home, makeDomain(8));

    Schema sch(home, {c0, c1, c2});
    Relation f = bitwisePlus(home, sch);

    Relation s = f.pickRandomSubrelation(home);
    EXPECT_TRUE(s.isSubsetOf(f));

    Relation t =
        f.pickRandomSubrelation(home, static_cast<size_t>(f.cardinality() / 2));
    EXPECT_TRUE(t.isSubsetOf(f));

    for (const Attribute& att : sch.asColumnDomains()) {
      Relation u = f.pickRandomSubrelation(home, {att});
      EXPECT_TRUE(u.isSubsetOf(f));
    }
  }
  EXPECT_EQ(0, home.zeroReferences());
}

TEST(Relvalue, variadic) {
  SpaceManager home;
  {
    AttributeDomain agent = makeDomain(4);

    // Maximum 4 entries in sender
    Attribute sender(home, agent, "Sender");

    Schema d(home, {sender});
    Relation unary(home, d);

    // unary.add(home,"zoe");
    // unary.add(home,"carl");
    unary.add(home, 0);
    unary.add(home, 1);

    print(home, unary, std::cout);

    Attribute receiver(home, agent, "Receiver");
    Schema q(home, {sender, receiver});
    Relation binary(home, q);

    // binary.add(home, "blue", "1");
    binary.add(home, 2, 0);
    print(home, binary, std::cout);
  }
  EXPECT_EQ(0, home.zeroReferences());
}
