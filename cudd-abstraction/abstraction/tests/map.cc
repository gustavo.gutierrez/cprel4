#include <gtest/gtest.h>

#include "bdd-abstraction.hh"

using namespace CuddAbstraction;

TEST(AttributeMapTest, creation) {
  SpaceManager home;
  {
    Attribute a0(home, makeDomain(4), "a0");
    Attribute a1(home, makeDomain(4), "a1");
    Attribute a2(home, makeDomain(4), "a2");

    Attribute b0(home, makeDomain(4), "b0");
    Attribute b1(home, makeDomain(4), "b1");
    Attribute b2(home, makeDomain(4), "b2");

    AttributeMap m;
    EXPECT_EQ(m.size(), 0);

    // <a0,b0> \in m
    m.add(a0, b0);
    m.add(a1, b1);
    m.add(a2, b2);
    EXPECT_EQ(m.size(), 3);

    EXPECT_TRUE(m.isInLHS(a0));
    EXPECT_TRUE(m.isInLHS(a1));
    EXPECT_TRUE(m.isInLHS(a2));

    EXPECT_TRUE(m.isInRHS(b0));
    EXPECT_TRUE(m.isInRHS(b1));
    EXPECT_TRUE(m.isInRHS(b2));

    EXPECT_EQ(b0, m.getRHSOf(a0));
    EXPECT_EQ(b1, m.getRHSOf(a1));
    EXPECT_EQ(b2, m.getRHSOf(a2));

    EXPECT_EQ(a0, m.getLHSOf(b0));
    EXPECT_EQ(a1, m.getLHSOf(b1));
    EXPECT_EQ(a2, m.getLHSOf(b2));

    AttributeMap id;
    id.add(a0, a0);
    id.add(a1, a1);
    id.add(a2, a2);

    EXPECT_TRUE(id.isIdentity());

    AttributeMap l({{a0, b0}, {a1, b1}, {a2, b2}});
    EXPECT_EQ(3, l.size());
    EXPECT_EQ(a0, l.getLHSOf(b0));

    // std::cerr << l;
  }
  EXPECT_EQ(0, home.zeroReferences());
}

TEST(AttributeMapTest, schema) {
  SpaceManager home;
  {
    Attribute a0(home, makeDomain(4), "a0");
    Attribute a1(home, makeDomain(4), "a1");
    Attribute a2(home, makeDomain(4), "a2");

    Attribute b0(home, makeDomain(4), "b0");
    Attribute b1(home, makeDomain(4), "b1");
    Attribute b2(home, makeDomain(4), "b2");

    Schema s0(home, {a0, a1, a2});

    AttributeMap m({{a0, b0}, {a1, b1}, {a2, b2}});
    EXPECT_TRUE(m.mapsAllAttributesIn(s0));

    Schema s1(home, {a0, a1});
    AttributeMap n({{a0, b0}, {a2, b2}});
    EXPECT_FALSE(n.mapsAllAttributesIn(s1));
    EXPECT_FALSE(m.mapsAllAttributesIn(s1));
  }
  EXPECT_EQ(0, home.zeroReferences());
}

TEST(AttributeMapTest, bdd) {
  SpaceManager home;
  {
    AttributeDomain d0 = makeDomain(4);
    AttributeDomain d1 = makeDomain(2);
    AttributeDomain d2 = makeDomain(1);

    Attribute a0(home, d0, "a0");
    Attribute a1(home, d1, "a1");
    Attribute a2(home, d2, "a2");

    Attribute b0(home, d0, "b0");
    Attribute b1(home, d1, "b1");
    Attribute b2(home, d2, "b2");

    AttributeMap m({{a0, b0}, {a1, b1}, {a2, b2}});

    std::vector<BDD> left, right;
    std::tie(left, right) = m.BDDMapping(home);
    EXPECT_EQ(left.size(), right.size());
  }
  EXPECT_EQ(0, home.zeroReferences());
}

class TestAttributeMap {
private:
  const AttributeMap mapping;

public:
  TestAttributeMap(void) {}
  TestAttributeMap(const AttributeMap& m0) : mapping(m0) {
    std::cerr << "ctor\n";
  }
  TestAttributeMap(const TestAttributeMap& obj) : mapping(obj.mapping) {
    std::cerr << "CCTOR\n";
  }
  void print(std::ostream& os) const {
    // os << mapping << "\n\n";
  }
};

TEST(AttributeMapTest, copy) {
  SpaceManager home;
  {
    AttributeDomain d0 = makeDomain(4);
    AttributeDomain d1 = makeDomain(2);
    AttributeDomain d2 = makeDomain(1);

    Attribute a0(home, d0, "a0");
    Attribute a1(home, d1, "a1");
    Attribute a2(home, d2, "a2");

    Attribute b0(home, d0, "b0");
    Attribute b1(home, d1, "b1");
    Attribute b2(home, d2, "b2");

    AttributeMap m({{a0, b0}, {a1, b1}, {a2, b2}});

    TestAttributeMap to(m);
    for (int i = 0; i < 10; i++) {
      TestAttributeMap ti(to);
      ti.print(std::cerr);

      TestAttributeMap tj(ti);
      tj.print(std::cerr);
    }
  }
  EXPECT_EQ(0, home.zeroReferences());
}