#ifndef __RELATION_HTML_FORMATTER_DECL_HH__
#define __RELATION_HTML_FORMATTER_DECL_HH__

#include <fstream>
#include <string>

#include "relation-decl.hh"
#include "formatter-decl.hh"

namespace CuddAbstraction {
/**
 * @brief Relation formatter to carry out the printing of relations
 */
class HtmlFormatter : public Formatter {
private:
  using Formatter::rel_;
  using Formatter::dg_;
  using Formatter::os_;

  /// Must Html include a body tag
  bool standalone_;
  /// Background color to use for the cells of the table
  std::string bgcolor_;

public:
  /// Constructs a formatter for relation @a r
  HtmlFormatter(const Relation& r, std::ostream& os, DomainGroup* dg = nullptr,
                const std::string& bgcolor = "#FFFFFF",
                bool standalone = false);
  /// Destructor
  virtual ~HtmlFormatter(void) {}
  /// Tells the formatter to generate standalone Html code
  void standalone(bool st);
  /// Sets the background color to use
  void bgcolor(const std::string& color);

private:
  Formatter::Decoder getDecoder(void) const;
  /**
   * @brief Prints an HTML representation of the relation schema to @a os
   *
   * The HTML output consists of an HTML table row specification with the
   * schema.
   */
  void printSchema(void) const;
  /**
   * @brief Outputs the tuples in the relation to @a os
   */
  void printTuples(const SpaceManager& home) const;

public:
  /**
   * @brief Outputs the contents of the relation in Html format to @a fileName
   */
  virtual void print(const SpaceManager& home) const;
};

/**
 * @brief Outputs the relation @a r to output stream @a os
 */
void printHtml(const SpaceManager& home, const Relation& rel, std::ostream& os,
               DomainGroup* dg = nullptr);
}

#endif