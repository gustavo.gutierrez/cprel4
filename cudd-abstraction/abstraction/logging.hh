#ifndef __LOGGING_HH__
#define __LOGGING_HH__

#include <string>
#include "version.hh"

#ifdef LOGGING_ENABLED

#include "timer-decl.hh"
#include <mutex>

// So that SpaceManager, Relation, Schema, etc are available.
#include "set-space-decl.hh"
#include "schema-decl.hh"
#include "relation-decl.hh"

#include "easylogging++.h"

/**
 * @brief Initializes the logging engine from the configuration in @a conf
 *
 * @a conf is a file name with a valid configuration. If the file does not
 *     exist, the logging engine is initialized with all the loggers disabled.
 */
void initLogging(const std::string& conf);
/**
 * @brief Share the logging configuration
 */
// el::base::type::StoragePointer sharedLoggingRepository();

namespace CuddAbstraction {
/**
 * @brief Logs statistics related to the BDD manager
 *
 * The log format is a simple comma separated list of values in the following
 * order:
 * - The label 'mgr'
 * - The date of build
 * - Memory in use (Megabytes)
 * - Number of garbage collections
 * - Total time of garbage collection (seconds)
 * - Number of cache look ups
 * - Number of cache hits
 * - Current version hash
 */
inline MAKE_LOGGABLE(SpaceManager, mgr, os) {
  os << "mgr," << buildDate() << "," << (mgr.ReadMemoryInUse() / 1048576.0)
     << "," << mgr.ReadGarbageCollections() << ","
     << (mgr.ReadGarbageCollectionTime() / 1000.0) << ","
     << mgr.ReadCacheLookUps() << "," << mgr.ReadCacheHits() << ","
     << getVersionHash();
  return os;
}
}

/**
 * @brief Logs statistics about a schema
 *
 * Information is written in a CSV format with the following fields:
 * - The label 'scharg'
 * - The date of build
 * - An identifier for the operation
 * - The position of the schema in the call
 * - The arity of the schema
 */
inline void logArgument(unsigned int opId, unsigned int pos,
                        const CuddAbstraction::Schema& sch) {
  LOG(WARNING) << "scharg," << CuddAbstraction::buildDate() << "," << opId
               << "," << pos << "," << sch.arity();
}

/**
 * @brief Logs statistics about a relation
 *
 * Information is written in a CSV format with the following fields:
 * - The label 'relarg'
 * - The date of build
 * - An identifier for the operation
 * - The position of the relation in the call
 * - The cardinality of the relation
 *
 * TODO: Missing cardinality of the relation information
 */
inline void logArgument(unsigned int opId, unsigned int pos,
                        const CuddAbstraction::Relation rel) {
  LOG(WARNING) << "relarg," << CuddAbstraction::buildDate() << "," << opId
               << "," << pos << "," << 0;
}

// Base case specialization
inline void logArguments(unsigned int, unsigned int) {}
/**
 * @brief Logs the argument of an operation
 */
template <typename T, typename... Args>
void logArguments(unsigned int opId, unsigned int pos, const T& arg,
                  Args... args) {
  logArgument(opId, pos, arg);
  logArguments(opId, pos + 1, args...);
}

//////////////////////
// Operation logger //
//////////////////////
class OperationLogger {

private:
  /////////////////
  // Static data //
  /////////////////
  /// Global counter of operation identifiers
  static unsigned int nextOpId_;
  /// Mutex to control the access to the counter
  static std::mutex mutex_;

private:
  /// Operation identifier the object is timing
  unsigned int opId_;
  /// Operation
  std::string operation_;
  // Number of arguments
  int args_;
  /// Timer for the object
  CuddAbstraction::util::ScopedTimer timer_;

public:
  /// Constructor
  template <typename... Args>
  OperationLogger(std::string operation, Args... args)
      : opId_(getIdForOperation())
      , operation_(std::move(operation))
      , args_(sizeof...(Args)) {
    if (args_ > 0)
      logArguments(opId_, 0, args...);
    timer_ = CuddAbstraction::util::ScopedTimer();
  }
  /// Destructor
  ~OperationLogger(void) {
    long long t = timer_.elapsed();
    LOG(INFO) << CuddAbstraction::buildDate() << "," << opId_ << ","
              << operation_ << "," << args_ << "," << t;
  }

private:
  /// Returns an operation identifier
  static unsigned int getIdForOperation(void);
};

/**
 * @brief Macro to log an operation and its arguments using a custom name
 *
 * The first argument of this macro must be a string that provides a name for
 * the operation in the log.
 */
#define LOG_OPERATION_NARGS(...)                                               \
  OperationLogger __this_name_should_be_UNIQUE__in_operation(__VA_ARGS__)
/**
 * @brief Macro to log an operation and its arguments
 *
 * The arguments of this macro are the parameters of the operation that will be
 * logged. This macro automatically assigns a name to the operation based on the
 * name of the function where it is called.
 */
#define LOG_OPERATION_ARGS(...)                                                \
  OperationLogger __this_name_should_be_UNIQUE__in_operation(_ELPP_FUNC,       \
                                                             __VA_ARGS__)

#define LOG_OPERATION                                                          \
  OperationLogger __this_name_should_be_UNIQUE__in_operation(_ELPP_FUNC)

#define NAMED_LOG_OPERATION(objname) OperationLogger objname(_ELPP_FUNC)

#define INIT_LOGGING(...) initLogging(__VA_ARGS__);

#else
////////////////////////////////////////////////////////////
// Macro definitions for logging when logging is disabled //
////////////////////////////////////////////////////////////
#define LOG_OPERATION_ARGS(...)
#define LOG_OPERATION_NARGS(...)
#define LOG_OPERATION
#define NAMED_LOG_OPERATION(objname)
#define INIT_LOGGING(...)

#endif
#endif