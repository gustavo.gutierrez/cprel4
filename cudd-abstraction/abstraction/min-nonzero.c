#include "min-nonzero.h"
#include <assert.h>
#include <util.h>
#include <cuddInt.h>

/*
  The implementation of the following function is an adaptation of
  Cudd_addFindMin present in the file cuddAddFind.c of the CUDD BDD library.
 */

DdNode* minNonzero(DdManager* dd, DdNode* f) {
  DdNode* t, *e, *res;

  statLine(dd);
  if (cuddIsConstant(f)) {
    return (f);
  }

  res = cuddCacheLookup1(dd, minNonzero, f);
  if (res != NULL) {
    return (res);
  }

  t = minNonzero(dd, cuddT(f));
  assert(cuddIsConstant(t) && cuddV(t) >= 0 &&
         "ADD contains negative discriminants");

  e = minNonzero(dd, cuddE(f));
  assert(cuddIsConstant(e) && cuddV(e) >= 0 &&
         "ADD contains negative discriminants");

  if (cuddV(t) == 0) {
    res = e;
  } else if (cuddV(e) == 0) {
    res = t;
  } else {
    res = (cuddV(t) <= cuddV(e)) ? t : e;
  }

  cuddCacheInsert1(dd, minNonzero, f, res);

  return (res);
}