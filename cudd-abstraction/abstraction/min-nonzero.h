#ifndef __MIN_NONZERO_H_
#define __MIN_NONZERO_H_

#include <stdio.h>
#include <cudd.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Returns the minimum non zero discriminant.
 * 
 * Given the ADD @a f this function tries to find the minimum discriminant
 * different from zero. If such discriminant exists, it is returned. In the case
 * that the only discriminant is zero then zero is returned.
 * 
 * @warning @a f is assumed to be an ADD with positive discriminants and no
 *     check about that is performed.
 * @todo There should be a function in CUDD that does this!
 */
DdNode* minNonzero(DdManager* mgr, DdNode* f);

#ifdef __cplusplus
}
#endif

#endif
