#ifndef __RELATION_DECL_HH__
#define __RELATION_DECL_HH__

#include "exception-decl.hh"
#include "set-space-decl.hh"
#include "integer-datum-decl.hh"
#include "schema-decl.hh"
#include "attribute-mapping-decl.hh"
#include "agg-relation-decl.hh"

#include "cuddObj.hh"
#include <vector>
#include <ostream>

namespace CuddAbstraction {

/**
 * @brief Exception thrown when the schema of a relation is invalid for an
 *     operation.
 */
class InvalidSchema : public Exception {
public:
  InvalidSchema(void) = delete;
  InvalidSchema(const char* location)
      : Exception(location, "Invalid schema") {}
};

/**
 * @brief Exception thrown when adding a tuple that does not match the arity of
 * the relation.
 */
class ArityMismatch : public Exception {
public:
  ArityMismatch(void) = delete;
  ArityMismatch(const char* location)
      : Exception(location, "Invalid arity") {}
};

/**
 * @brief Exception thrown when expecting a schema which attributes must be part
 * of another schema.
 */
class ExpectingContainedDomains : public Exception {
public:
  ExpectingContainedDomains(void) = delete;
  ExpectingContainedDomains(const char* location)
      : Exception(location,
                  "Attempt to operate domains the need to be contained") {}
};

/**
 * @brief Exception thrown when expecting two schemas to not share any
 *     attribute.
 */
class ExpectingDisjointSchemas : public Exception {
public:
  ExpectingDisjointSchemas(void) = delete;
  ExpectingDisjointSchemas(const char* location)
      : Exception(location,
                  "Attempt to operate domains that need to be disjoint") {}
};

/**
 * @brief Exception thrown when attempting to select a subrelation from an empty
 *     relation
 */
class SelectionOnEmptyRelation : public Exception {
public:
  SelectionOnEmptyRelation(void) = delete;
  SelectionOnEmptyRelation(const char* location)
      : Exception(location,
                  "Attempt to select a subrelation from empty relation") {}
};
// Forward declaration for relation statistics
class RelationStats;
// Forward declaration of aggregated relation
class AggRelation;
// Forward declaration of unique abstractor
class UniqueAbstractor;

/**
 * @brief Representation of a relation.
 *
 * A relation has two parts:
 * - A schema that defines its attributes with their respective domains
 * - The relation data that contains the tuples that belong to the relation.
 *
 * Objects of this class use BDDs to represent the relation data and provide
 * an implementation for the basic relation operations in terms of the BDD
 * representations.
 */
class Relation {
  /**
   * This friendship relation is used only for statistics purposes that require
   * access to the private and protected fields.
   */
  friend class RelationStats;
  friend class AggRelation;

private:
  /// Schema of the relation
  Schema schema_;
  /// Relation data
  BDD data_;

protected:
  /// Constructor from a BDD and a domain \a sch
  Relation(BDD f, const Schema& sch);

public:
  /// @brief Constructors
  ///@{
  // Default constructor
  Relation(void);
  /// Constructs an empty relation with schema \a sch
  Relation(const SpaceManager& home, const Schema& sch);
  /**
   * @brief Constructs an empty relation with the schema defined by the set of
   *     attributes in @a sch
   *
   * The order in the attributes does matter. This constructor is a shortcut
   * for:
   * \@code
   * Schema s(home, {a, b, c});
   * Relation r(home, s);
   * @endcode
   */
  Relation(const SpaceManager& home, const std::vector<Attribute>& sch);
  /// Copy constructor
  Relation(const Relation& r);
  /**
   * @brief Assignment operator.
   *
   * @warning Throws an exception InvalidSchema if this relation has a different
   *     schema than the assigned relation.
   */
  Relation& operator=(const Relation& r);
  ///@}

public:
  /// Cast operator to BDDs
  operator BDD(void) const;
  /// Returns the schema of this relation
  const Schema& schema(void) const;
  /// Adds the tuple \a t to the relation
  void add(const SpaceManager& home, const NumericTuple& t);
  /**
   * @brief Adds the tuple formed by @a args to the relation
   *
   * - @a args can only be integer numbers that must be valid according to the
   *   attribute domain of the element.
   * - The attribute that corresponds to the i-th  argument is assumed to
   *   correspond to the i-th attribute of the schema of the relation.
   */
  template <typename... Args>
  void add(const SpaceManager& home, Args... args);
  /**
   * @brief Adds the tuple described by the range [@a begin, @a end) to the
   *     relation.
   *
   * - Dereferencing the iterator must lead to an value of type IntegerDatum.
   */
  template <typename It>
  void addFromIterator(const SpaceManager& home, It begin, It end);
  /**
   * @brief Fills the relation with the data contained in the file @a fname.
   *
   * - The file is a text file with one tuple per line.
   * - The elements of the tuples are separated by a space and must be integers.
   * - It is assumed that the order of the elements in each line is the same as
   *   the order of declaration of value domains in the relation domain
   * - Returns the number of lines read from the file
   */
  int readFromFile(const SpaceManager& home, const char* fname);
  /**
   * @brief Writes the contents of this relation to file @a fname.
   *
   * Uses the same format expected by @a readFromFile.
   */
  void writeToFile(const SpaceManager& home, const char* fname) const;
  /// Removes the tuple \a t from the relation
  void remove(const SpaceManager& home, const NumericTuple& t);
  /**
   * @brief Removes the tuple representing @a args from the relation
   *
   * The parameters can be only unsigned integers
   */
  template <typename... Args>
  void remove(const SpaceManager& home, Args... args);

public:
  /**
   * @brief Computes the union of this relation and \a r
   *
   * @warn Throws an exception InvalidSchema if @a r is on a different schema
   *     than this relation.
   */
  Relation unionWith(const Relation& r) const;
  /**
   * @brief Computes the intersection of this relation and \a r
   *
   * @warning Throws an exception InvalidSchema if @a r is on a different schema
   *     than this relation.
   */
  Relation intersectWith(const Relation& r) const;
  /**
   * @brief Computes the difference between this relation and \a r
   *
   * The semantics of this operation is defined by looking at relations as sets.
   *
   * @warning Throws an exception InvalidSchema if @a r is on a different schema
   *     than this relation.
   */
  Relation differenceWith(const Relation& r) const;
  /**
   * @brief Computes the complement of this relation
   */
  Relation complement(void) const;
  /**
   * @brief Tests whether this relation is empty
   *
   * A relation is empty when it contains no tuples.
   */
  bool isEmpty(void) const;
  /**
   * @brief  Tests whether this is full.
   *
   * A relation is full when its set of tuples is the Cartesian product of the
   * domain of the attributes in its schema.
   */
  bool isFull(void) const;
  /**
   * @brief Tests whether this relation is a subset of relation \a r
   *
   * @warning Throws an exception DomainMismatch if @a r is on a different
   *     schema than this relation.
   */
  bool isSubsetOf(const Relation& r) const;
  /**
   * @brief Tests whether this relation and \a r are disjoint
   *
   * @warning Throws an exception DomainMismatch if @a r is on a different
   *     schema than this relation.
   */
  bool isDisjointWith(const Relation& r) const;
  /// Returns the cardinality of this relation
  double cardinality(void) const;
  /// Returns the maximum cardinality this relation can have
  double maxCardinality(void) const;
  /// Returns the arity of this relation
  size_t arity(void) const;
  /// Returns if this relation and relation \a r are the same
  bool sameAs(const Relation& r) const;

public:
  /// Tests whether this relation and relation \a r have the same domain
  bool sameSchemaAs(const Relation& r) const;
  /**
   * @brief Returns a copy of this relation with a new schema @a s.
   *
   * The correspondence between the schema of this and the new schema is
   * specified by @a m.
   *
   * @warning Throws an exception InvalidAttributeMap if the mapping represented
   *     by @a m does not map the schema of this relation to @a s completely.
   */
  Relation withSchema(const SpaceManager& home, const AttributeMap& m,
                      const Schema& s) const;
  /**
   * @brief Returns the relation resulting from taking this relation and
   *     projecting it to consider only the attributes that belong to the schema
   *     @a newSchema.
   */
  Relation projectTo(const SpaceManager& home, const Schema& newSchema) const;

private:
  /**
   * @brief Abstracts the attributes in @a attributesToAbstract by unique
   *     quantification.
   *
   * The schema of the resulting relation is:
   * this->schema() \ attributesToAbstract
   *
   * \todo this method is private because it is more handy to specify  the
   * list of abstracted attributes by using a list of attributes which is
   * implicitly transformed to a vector of attributes. In any case it is more
   * convenient to have both public.
   */
  Relation uniqueAbstract(const SpaceManager& home,
                          const Schema& attributes) const;

public:
  /**
   * @brief Abstracts the attributes in @a attributesToAbstract by unique
   *     quantification.
   *
   * The schema of the resulting relation is:
   * this->schema() \ attributesToAbstract
   */
  Relation
  uniqueAbstract(const SpaceManager& home,
                 const std::vector<Attribute>& attributesToAbstract) const;
  /**
   * @brief Abstracts the attributes in @a attributesToAbstract by unique
   *     quantification.
   *
   * This is the fast version of the unique abstraction. Using it requires some
   * properties from the variable order among the attributes.
   *
   * These properties are not checked so it is up to the caller to ensure them.
   *
   * The schema of the resulting relation is:
   * this->schema() \ attributesToAbstract
   */
  Relation
  uniqueAbstractFast(const SpaceManager& home,
                     const std::vector<Attribute>& attributesToAbstract) const;
  /**
   * @brief Tests if the fast implementation of unique abstraction can be used
   * to abstract @a attributesToAbstract from this relation.
   *
   * At the end, this tests if a call to @a uniqueAbstract can be safely called
   * to uniquely abstract @a attributesToAbstract.
   *
   * @warning{This method does not make any check to test whether @a
   *          attributesToAbstract can be uniquely abstracted.}
   */
  bool canUseUniqueAbstractFast(
      const SpaceManager& home,
      const std::vector<Attribute>& attributesToAbstract) const;
  /**
   * @brief Tests if the fast implementation of unique abstraction can be used
   * to compute the unique relation with respect to the set of attributes @a
   * attributes.
   *
   * @warning{This method does not make any check to test whether @a
   *          attributesToAbstract can be uniquely abstracted.}
   */
  bool canUseFastUniqueWithRespectTo(const SpaceManager& home,
                                     const Schema& attributes) const;
  /**
   * @brief Compute a subrelation which is unique when the attributes in @a
   *     attributes are the only ones considered.
   *
   * Example, given the following relation:
   *
   * |----|----|----|----|
   * | C0 | C1 | C2 | C3 |
   * |----|----|----|----|
   * |  0 |  1 |  2 |  3 |
   * |  4 |  1 |  2 |  3 |
   * |  9 |  8 |  2 |  3 |
   * |----|----|----|----|
   *
   * The schema of the relation D is {C0,C1,C2,C3}. Now suppose we want to find
   * out which is the subrelation that contains unique tuples when considering
   * the attributes {C1,C2,C3}. To do that we call this method with @a
   * attributes set to {C1,C2,C3}. For this particular example we will get the
   * relation:
   *
   * |----|----|----|----|
   * | C0 | C1 | C2 | C3 |
   * |----|----|----|----|
   * |  9 |  8 |  2 |  3 |
   * |----|----|----|----|
   *
   * The reason is that when considering only the provided attributes, the first
   * two tuples become equal and hence non unique.
   *
   * The @a useFastUniqueAbstract implementation instructs this operation to use
   * the fast implementation of the unique abstraction operation. For this to
   * work there is an extra condition that must be satisfied by the relation.
   * See @a canUseUniqueAbstract for more details. For optimization reasons no
   * check is performed internally.
   */
  Relation uniqueWithRespectTo(const SpaceManager& home,
                               const Schema& attributes,
                               bool useFastUniqueAbstract = false) const;
  /**
   * @brief Computes the relation with the unique tuples in this relation when
   *     considering only the set of attributes @a attributes.
   *
   * This operation is the same as the above but takes a vector of attributes
   * instead of a schema.
   */
  Relation uniqueWithRespectTo(const SpaceManager& home,
                               const std::vector<Attribute>& attributes,
                               bool useFastUniqueAbstract = false) const;
  /**
   *
   *
   * @brief Computes the relation with the unique tuples in this relation when
   * considering only the set of attributes @a attributesToAbstract.
   *
   * This operation is the same as the above ones but uses a different
   * approach to compute the result. In consequence it requires an abstractor
   * that handles the creation of an extra set of attributes and an equality
   * relation. The appropriate abstractor is returned by @a
   * createUniqueAbstractor
   *
   */
  Relation uniqueWithRespectTo(const SpaceManager& home,
                               const UniqueAbstractor& abstractor) const;
  /**
   * @brief Constructs a unique abstractor object to find the unique
   * subrelation from this relation with respect to @a attributes.
   *
   * @warn The unique abstractor creates a new set of attributes.
   */
  UniqueAbstractor
  createUniqueAbstractor(SpaceManager& home,
                         const std::vector<Attribute>& attributes) const;

public:
  /// Abstracts the attributes in @a attributes by existential quantification
  Relation existAbstract(const SpaceManager& home,
                         const Schema& attributes) const;
  /**
   * @brief Computes the cross product of this relation with the full relation
   *     formed from @a sch
   */
  Relation crossWith(const SpaceManager& home, const Schema& sch) const;
  /// Computes the cross product of this relation with relation @a r
  Relation crossWith(const SpaceManager& home, const Relation& r) const;
  /**
   * @brief Computes the join of this relation with relation @a r
   *
   * The join operation is performed assuming that the columns are the ones
   * represented by the same column domains in both @a this and @a r relations.
   *
   * Side effects to expect:
   * - If @a this and @a r does not have any column domain in common then this
   *   operation is equivalent to cross product of the relations.
   * - If @a this and @a r have the same relation domain, that is, they have in
   *   common all the column domains then this operation is equivalent to the
   *   intersection of the relations.
   */
  Relation joinWith(const SpaceManager& home, const Relation& r) const;
  /**
   * @brief Computes the join of this relation and @a r then projects the result
   *     on @a project schema
   *
   * This is an optimization that performs join followed by a projection.
   *
   * The following operations
   *
   *  R = x.joinWith(home,y)
   *  P = R.projectTo(home, sch)
   *
   *  Are equivalent to:
   *  P = x.joinWithAndProject(home,y,sch)
   */
  Relation joinWithAndProject(const SpaceManager& home, const Relation& r,
                              const Schema& project) const;
  /**
   * @brief Computes the composition of this relation with relation @a r
   *
   * The composition is performed assuming that the columns to compose on are
   * the ones that have the same columns domains in both @a this and @a r
   * relations.
   */
  Relation composeWith(const SpaceManager& home, const Relation& r) const;
  /// Returns the contents of the relation as a collection of numeric tuples
  std::vector<NumericTuple> asTuples(const SpaceManager& home) const;

public:
  /**
   * @brief Picks a tuple randomly from the relation
   */
  NumericTuple pickRandomTuple(void) const;
  /**
   * @brief Returns a subrelation of cardinality one of this relation
   *
   * TODO: throw an exception if this is the empty relation
   */
  Relation pickAnyUnarySubrelation(const SpaceManager& home) const;
  /**
   * @brief Returns a random subrelation of this relation
   */
  Relation pickRandomSubrelation(const SpaceManager& home) const;
  /**
   * @brief Returns a random subrelation with @a n tuples.
   *
   * If the cardinality of this relation is less than @a n then a copy of this
   * relation is returned.
   */
  Relation pickRandomSubrelation(const SpaceManager& home, size_t n) const;
  /**
   * @relates Returns a subrelation of this relation based on the values of the
   *     attributes in @a a.
   *
   * The attributes in @a a must be a subset of the attributes in the schema of
   * this relation.
   *
   * TODO: check this condition with an assertion.
   * TODO: Document this method better!
   */
  Relation pickRandomSubrelation(const SpaceManager& home,
                                 const std::vector<Attribute>& a) const;

public:
  /// Creates an empty relation with schema @a sch
  static Relation createEmpty(const SpaceManager& home, const Schema& sch);
  /// Creates a full relation with domain @a sch
  static Relation createFull(const SpaceManager& home, const Schema& dom);

public:
  /**
   * @brief Outputs this relation to a file with name @a fileName in dot format
   *
   * - If @a asADD is true then the equivalent ADD is printed instead of the
   *   BDD.
   * - @a funcName indicates the name of the root node
   */
  void toDot(const SpaceManager& home, const char* fileName,
             const char* funcName, bool asADD = true) const;

public:
  /// Returns representation statistics
  RelationStats getStats(void) const;
  /**
   * @brief Utility operation to inspect the BDD representation of this
   *     relation.
   *
   * The operations prints to standard output the following information:
   *
   * - Number of BDD nodes used
   *
   * - Number of represented miterms
   *
   * - Cover representation of the BDD.
   *
   * @a label is an optional parameter used printed to distinguish from other
   *     inspected relations.
   * @a printCover can be set to true to actually print the cover of the BDD.
   */
  void bddInspect(const std::string& label = "Relation",
                  bool printCover = false) const;

private:
  // Some external operations work directly on relation values as BDDs and are
  // able to use BDD operations for fast relation creation. Those operations
  // make use of protected operations and are therefore listed as friends of
  // this class.

  // Operations to create ground relations
  friend Relation lessOrEqual(const SpaceManager& home, const Schema& sch);

  friend Relation lessOrEqual(const SpaceManager& home, const Attribute& d,
                              const IntegerDatum& c);

  friend Relation lessThan(const SpaceManager& home, const Schema& sch);

  friend Relation lessThan(const SpaceManager& home, const Attribute& d,
                           const IntegerDatum& c);

  friend Relation equal(const SpaceManager& home, const Schema& sch);

  friend Relation bitwisePlus(const SpaceManager& home, const Schema& sch);

  friend Relation bitwisePlus(const SpaceManager& home, const Attribute& left,
                              const IntegerDatum& c, const Attribute& result);

  friend Relation arithPlus(const SpaceManager& home, const Attribute& left,
                            const IntegerDatum& c, const Attribute& result);

  friend Relation arithPlus(const SpaceManager& home, const Schema& sch);
};

/**
 * @brief Equality operator overload for relations
 */
bool operator==(const Relation& lhs, const Relation& rhs);
}
#endif