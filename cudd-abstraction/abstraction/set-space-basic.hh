#ifndef __SET_SPACE_BASIC_HH__
#define __SET_SPACE_BASIC_HH__

#include "set-space-decl.hh"
#include <cassert>

namespace CuddAbstraction {

inline int SpaceManager::level(DdNode* d) const {
  return Cudd_ReadPerm(getManager(), Cudd_NodeReadIndex(d));
}

inline BDD SpaceManager::high(const BDD& r) const {
  DdNode* d = r.getNode();
  DdNode* res = Cudd_T(d);
  res = Cudd_NotCond(res, Cudd_IsComplement(d));
  return BDD(*this, res);
}

inline BDD SpaceManager::low(const BDD& r) const {
  DdNode* d = r.getNode();
  DdNode* res = Cudd_E(d);
  res = Cudd_NotCond(res, Cudd_IsComplement(d));
  return BDD(*this, res);
}

inline BDD SpaceManager::regular(const BDD& r) const {
  return BDD(*this, r.getRegularNode());
}

inline unsigned int SpaceManager::var2level(const BDD& r) const {
  return ReadPerm(r.NodeReadIndex());
}

inline BDD SpaceManager::topVar(const BDD& r) const {
  return ReadVars(r.NodeReadIndex());
}

inline int SpaceManager::value(const ADD& r) const {
  assert(Cudd_IsConstant(r.getNode()));
  return static_cast<int>(Cudd_V(r.getNode()));
}
}
#endif
