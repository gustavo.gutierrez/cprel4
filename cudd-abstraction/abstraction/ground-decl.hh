#ifndef __GROUND_DECL_HH__
#define __GROUND_DECL_HH__

#include "set-space-decl.hh"
#include "attribute-decl.hh"
#include "schema-decl.hh"
#include "relation-decl.hh"

namespace CuddAbstraction {

/**
 * \brief Exception thrown by operations that require the domain of a binary
 * relation
 */
class ExpectingBinaryRelationDomain : public Exception {
public:
  ExpectingBinaryRelationDomain(void) = delete;
  ExpectingBinaryRelationDomain(const char* location)
      : Exception(location, "Expecting the domain of a binary relation") {}
};
/**
 * \brief Exception thrown by operations that require the domain of a ternary
 * relation
 */
class ExpectingTernaryRelationDomain : public Exception {
public:
  ExpectingTernaryRelationDomain(void) = delete;
  ExpectingTernaryRelationDomain(const char* location)
      : Exception(location, "Expecting the domain of a ternary relation") {}
};
/**
 * @brief Returns a relation value  with domain @a d that contains all the
 * possible tuples <x,y,z> such that z is the bitwise addition of x and
 * y. Also, x belongs to d[0], y belongs to d[1] and z belongs to d[2].
 *
 * If @a d contains value domains of different lengths or is not a ternary
 * domain then a DomainMismatch exception is thrown.
 *
 * @warn Some tuples of the returned relation might look wrong. That is usually
 * when overflow occurs and this is the reason to call this function bitwise
 * addition.
 */
Relation bitwisePlus(const SpaceManager& home, const Schema& d);
/**
 * @brief Returns a relation value with domain {@left, @a result} that contains
 * all the possible tuples <x,y> such that y is the bitwise addition of x and
 * the constant @a c. Also, x belongs to domain @a left and y belongs to domain
 * @a result.
 *
 * A DomainMismatch exception is thrown if @a left and @a result are not of the
 * same size.
 *
 * Tuples in which overflow occurs are part of the result.
 */
Relation bitwisePlus(const SpaceManager& home, const Attribute& left,
                     const IntegerDatum& c, const Attribute& result);
/**
 * @brief Returns a relation value with domain @a d that contains all the
 * possible tuples <x,y,z> such that z is the arithmetic addition of x and
 * y. Also, x belongs to d[0], y belongs to d[1] and z belongs to d[2].
 *
 * A DomainMismatch exception is thrown if:
 *  - all the three value domains in @a dom are not of the same size, or
 *  - @a d is not a ternary domain, or
 *  - the size of the domains is not enough to represent @a c
 *
 * Tuples in which overflow occurs are not part of the result.
 */
Relation arithPlus(const SpaceManager& home, const Schema& d);
/**
 * @brief Returns a relation value with domain {@left, @a result} that contains
 * all the possible tuples <x,y> such that y is the arithmetic addition of x and
 * the constant @a c. Also, x belongs to domain @a left and y belongs to domain
 * @a result.
 *
 * A DomainMismatch exception is thrown if:
 *  - the value domains @a left and @a result are not of the same size, or
 *  - the size of the domains is not enough to represent @a c
 *
 * Tuples in which overflow occurs are not part of the result.
 */
Relation arithPlus(const SpaceManager& home, const Attribute& left,
                   const IntegerDatum& c, const Attribute& result);
/**
 * @brief Returns a relation value with domain @a d that contains all the
 * possible tuples <x,y> such that x is less or equal than y. Also, x belongs to
 * d[0] and y belongs to d[1].
 *
 * A DomainMismatch exception is thrown if the value domains in @a d are not of
 * the same size or if @a d is not a binary domain.
 */
Relation lessOrEqual(const SpaceManager& home, const Schema& d);
/**
 * @brief Returns a relation value with domain {@a dom} that contains all the
 * possible tuples <x> such that x is less or equal than constant @a c. Also, x
 * belongs to dom.
 *
 * A DomainMismatch exception is thrown if the size of @a dom is not enough to
 * represent @a c
 */
Relation lessOrEqual(const SpaceManager& home, const Attribute& dom,
                     const IntegerDatum& c);
/**
 * @brief Returns a relation value with domain @a d that contains all the
 * possible tuples <x,y> such that x is less than y. Also, x belongs to
 * d[0] and y belongs to d[1].
 *
 * A DomainMismatch exception is thrown if the value domains in @a d are not of
 * the same size or if @a d is not a binary domain.
 */
Relation lessThan(const SpaceManager& home, const Schema& d);
/**
 * @brief Returns a relation value with domain {@a dom} that contains all the
 * possible tuples <x> such that x is less than constant @a c. Also, x belongs
 *to
 * @a dom.
 *
 * A DomainMismatch exception is thrown if the size of @a dom is not enough to
 * represent @a c
 */
Relation lessThan(const SpaceManager& home, const Attribute& dom,
                  const IntegerDatum& c);
/**
 * @brief Returns a relation value with domain @a d that contains all the
 * possible tuples <x,y> such that x is greater or equal than y. Also, x belongs
 * to d[0] and y belongs to d[1].
 *
 * A DomainMismatch exception is thrown if the value domains in @a d are not of
 * the same size or if @a d is not a binary domain.
 */
Relation greaterOrEqual(const SpaceManager& home, const Schema& d);
/**
 * @brief Returns a relation value with domain {@a dom} that contains all the
 * possible tuples <x> such that x is greater or equal than constant @a c. Also,
 * x belongs to dom.
 *
 * A DomainMismatch exception is thrown if the size of @a dom is not enough to
 * represent @a c
 */
Relation greaterOrEqual(const SpaceManager& home, const Attribute& dom,
                        const IntegerDatum& c);
/**
 * @brief Returns a relation value with domain @a d that contains all the
 * possible tuples <x,y> such that x is greater than y. Also, x belongs to
 * d[0] and y belongs to d[1].
 *
 * A DomainMismatch exception is thrown if the value domains in @a d are not of
 * the same size or if @a d is not a binary domain.
 */
Relation greaterThan(const SpaceManager& home, const Schema& d);
/**
 * @brief Returns a relation value with domain {@a dom} that contains all the
 * possible tuples <x> such that x is greater than constant @a c. Also,
 * x belongs to dom.
 *
 * A DomainMismatch exception is thrown if the size of @a dom is not enough to
 * represent @a c
 */
Relation greaterThan(const SpaceManager& home, const Attribute& dom,
                     const IntegerDatum& c);
/**
 * @brief Returns a relation value with domain @a d that contains all the
 * possible tuples <x,y> such that x is equal to y. Also, x belongs to d[0] and
 * y belongs to d[1].
 *
 * A DomainMismatch exception is thrown if the value domains in @a d are not of
 * the same size or if @a d is not a binary domain.
 */
Relation equal(const SpaceManager& home, const Schema& d);
/**
 * @brief Returns a relation value with domain @a d that contains all the
 * possible tuples <x,y> such that x is different from y. Also, x belongs to
 *d[0] and
 * y belongs to d[1].
 *
 * A DomainMismatch exception is thrown if the value domains in @a d are not of
 * the same size or if @a d is not a binary domain.
 */
Relation different(const SpaceManager& home, const Schema& d);
}
#endif