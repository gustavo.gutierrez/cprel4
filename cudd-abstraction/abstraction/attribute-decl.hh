#ifndef __ATTRIBUTE_DECL_HH__
#define __ATTRIBUTE_DECL_HH__

#include "exception-decl.hh"
#include "integer-datum-decl.hh"
#include "set-space-decl.hh"
#include "domain-decl.hh"

#include <vector>
#include <string>

namespace CuddAbstraction {

/**
 * @brief Exception thrown when the attribute representation is not enough to
 * represent an attribute
 */
class InadequateDomainRepresentation : public Exception {
public:
  InadequateDomainRepresentation(void) = delete;
  InadequateDomainRepresentation(const char* location)
      : Exception(location, "Attempt use a domain that cannot be represented "
                            "with the current integer representation") {}
};

/**
 * @brief Exception thrown when using invalid data of a domain
 */
class InvalidDomainData : public Exception {
public:
  InvalidDomainData(void) = delete;
  InvalidDomainData(const char* location)
      : Exception(location, "Invalid data") {}
};

class Attribute {
private:
  /// Domain associated with this attribute
  AttributeDomain domain_;
  /// Indexes of the BDD variables used to represent values in this domain
  SpaceManager::BDDVarDomain indices_;
  /// Cube of variables representing the values in this domain
  BDD cube_;
  /// Attribute name
  std::string name_;

private:
  void constructor(SpaceManager& home);

public:
  // Default constructor
  Attribute(void);
  /**
   * @brief Constructs a value domain from a list of indices \a i that identify
   *     BDD variables in \a home.
   *
   * @warning This constructor needs to be used carefully. It skips the domain
   *     allocator and therefore can use BDD variables that are already in use.
   *
   * The way of creating a domain is:
   * auto d = home.alloc(16);
   * Attribute vd(home,d);
   */
  Attribute(SpaceManager& home, const std::vector<int>& vars,
            AttributeDomain domain, const char* name = "No-name");
  /**
   * @brief Constructs a value domain of the given @a length.
   *
   * The BDD variables used for this attribute are allocated and guaranteed to
   * be different from the ones of any other attribute. @a interleaved indicates
   * whether the variables are allocated from the interleaved region (i.e.
   * interleaved with other variables allocated in a similar way) or are from
   * the linear region.
   */
  Attribute(SpaceManager& home, AttributeDomain domain, bool interleaved = true,
            const char* name = "No-name");
  /// Copy constructor
  Attribute(const Attribute& d);
  /// Assignment operator
  Attribute& operator=(const Attribute& right);

public:
  /// Returns the domain corresponding to this attribute
  AttributeDomain domain(void) const;
  /// Returns the number of BDD variables used to represent this attribute
  size_t reprSize(void) const;
  /**
   * Returns the indices of the BDD variables used to represent this attribute
   */
  const SpaceManager::BDDVarDomain& getIndices(void) const;

public:
  /// Encodes @a d as a value of this attribute
  BDD asBDD(const SpaceManager& home, const IntegerDatum& d) const;
  /**
   * @brief Decodes a datum from a minterm representation
   *
   * \a minterm is assumed to have the same size as the \a indices_ attribute.
   * also there is an index correspondence between the values in the miterm and
   * the values in indices_.
   *
   * TODO: This method produces an error if the minterm contains elements
   * different from 0 or 1.
   */
  IntegerDatum asInteger(/*const SpaceManager& home,*/ int* minterm) const;
  /**
   * @brief Selects the elements in \a minterm that correspond to this domain.
   *
   * This method assumes that minterm can be indexed by the indices stored
   * in indices_. It also preserves the order in which the elements are stored
   * with respect to the indices.
   */
  std::vector<int> select(int* minterm) const;
  /**
   * @brief Returns a cube of the BDD variables used to represent this
   * attribute.
   *
   * \todo This method may crash if any index cannot be converted (without
   * loosing data) from and unsigned int to an int.
   */
  BDD asCube(void) const;

  /**
   * @brief Returns a vector with the BDD variables used to represent this
   *     attribute.
   */
  std::vector<BDD> asVectorOfVariables(const SpaceManager& home) const;

public:
  /// Returns if this and \a d are the same domain
  bool operator==(const Attribute& d) const;
  /// Returns if this and \a d are the same domain
  bool operator!=(const Attribute& d) const;
  /**
   * @brief Returns if this attribute is less or equal than attribute @a a
   *
   * This operation does not have a direct sense on attributes. It is only
   * useful for the purpose of storing attributes in associative data structures
   * like maps.
   */
  bool operator<(const Attribute& a) const;

public:
  /// Returns the name of the attribute
  const std::string& name(void) const;
  /// Returns a debug string with the attribute information
  std::string debugInfo(bool varInfo = false) const;
  /// Sets the attribute name to @a n
  void name(std::string n);
  /**
   * @brief Checks if this attribute has the same AttributeDomain as @a other.
   *
   * Two value domains have the same attribute domain if they are two
   * representations of the same set of elements. This is important to keep
   * consistency during renaming.
   */
  bool hasSameAttributeDomainAs(const Attribute& other) const;
  /**
   * @brief Fill the vector @a names with an identifier for each variable in
   *     this attribute
   *
   * The size of @a names is the number of BDD variables in the manager and is
   * assumed to have place for all the variables used to represent this
   * attribute.
   */
  void fillWithBDDVarNames(std::vector<std::string>& names) const;
};

/**
 * @brief Initialize @a args as attributes.
 *
 * Every attribute has a new associated domain of cardinality @a card and the
 * attributes are guaranteed to be represented using interleaved BDD variables.
 */
template <typename... Args>
void initializeDomains(SpaceManager& home, int card, Args&... args);
/// Outputs a representation of domain @a d to @a os
std::ostream& operator<<(std::ostream& os, const Attribute& d);
/// Tests if attributes @a d and @a e have the same representation size
bool sameReprSize(const Attribute& d, const Attribute& e);
/**
 * @brief Tests if attributes @a d, @a e and @a f have all the same
 *     representation size
 */
bool sameReprSize(const Attribute& d, const Attribute& e, const Attribute& f);
}

#endif
