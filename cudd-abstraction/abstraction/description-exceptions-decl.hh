#ifndef __DESCRIPTION_EXCEPTION_DECL_HH__
#define __DESCRIPTION_EXCEPTION_DECL_HH__

#include "exception-decl.hh"

namespace CuddAbstraction {
/**
 * @brief Exception thrown when a column domains is included in a description
 *     but for some reason it cannot be there.
 */
class InvalidDomainInDescription : public Exception {
public:
  InvalidDomainInDescription(void) = delete;
  InvalidDomainInDescription(const char* location)
      : Exception(location, "Invalid domain in description") {}
};
/**
 * @brief Exception thrown when trying to relate two column domains as part of a
 *     description but a requirement on their size is not satisfied.
 */
// class InvalidDomainSize : public Exception {
// public:
//   InvalidDomainSize(void) = delete;
//   InvalidDomainSize(const char *m)
//     : Exception(m) {}
// };
/**
 * @brief Exception thrown when attempting to create a description that is not
 * bijective
 */
class NonBijectiveDescription : public Exception {
public:
  NonBijectiveDescription(void) = delete;
  NonBijectiveDescription(const char* location)
      : Exception(location, "Attempt to create a non bijective description") {}
};
}

#endif