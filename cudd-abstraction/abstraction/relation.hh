#ifndef __RELATION_HH__
#define __RELATION_HH__

#include "relation-decl.hh"
#include "stats-decl.hh"
#include "logging.hh"
#include "formatter-decl.hh"
#include "format.h"

#include <string>
#include <sstream>
#include <fstream>
#include <iomanip>
#include <algorithm>

namespace CuddAbstraction {
class UniqueAbstractor {
private:
  const Schema relSch_;
  const Schema toAbstract_;
  const Schema extra_;
  const BDD equality_;
  const AttributeMap map_;
  const Schema tempRelSchema_;

public:
  UniqueAbstractor(SpaceManager& home, const Schema& relSch,
                   const std::vector<Attribute>& attributes)
      : relSch_(relSch)
      , toAbstract_(relSch_.differenceWith(home, Schema(home, attributes)))
      , extra_(initExtra(home, toAbstract_))
      , equality_(buildEquality(home, toAbstract_, extra_))
      , map_(buildMap(home, relSch_, toAbstract_, extra_))
      , tempRelSchema_(
            relSch_.differenceWith(home, toAbstract_).unionWith(home, extra_)) {
    /**
     * TODO: print a message if the equality relation is not produced with
     * interleaved variables as this is very poorly represented with BDDs.
     */
  }

  Relation relOnTempAttributes(const SpaceManager& home,
                               const Relation& r) const {
    Assert<GenericException>(r.schema().sameAs(relSch_),
                             "UniqueAbstractor::relOnTempAttributes");
    return r.withSchema(home, map_, tempRelSchema_);
  }

  BDD disequality(void) const { return ~equality_; }

  BDD extra(void) const { return extra_.asCube(); }

  void debugInfo(void) const {
    auto vars =
        toAbstract_.asCube().SupportSize() + extra_.asCube().SupportSize();
    auto minterms = equality_.CountMinterm(vars);
    auto nodes = equality_.nodeCount();
    fmt::print("Equality relation has {} minterms and {} nodes\n", minterms,
               nodes);
  }

private:
  static Schema initExtra(SpaceManager& home, const Schema& toAbstract) {
    // Create a schema with matching attributes for the ones in toAbstract
    std::vector<Attribute> ex(toAbstract.arity());
    for (size_t i = 0; i < toAbstract.arity(); i++) {
      const AttributeDomain& d = toAbstract.asColumnDomains()[i].domain();
      ex[i] = Attribute(home, d, true);
    }
    return Schema(home, ex);
  }

public:
  static BDD buildEquality(const SpaceManager& home, const Schema& s,
                           const Schema& sprime) {
    Assert<ArityMismatch>(s.arity() == sprime.arity(),
                          "UniqueAbstractor::buildEquality");
    Assert<ExpectingDisjointSchemas>(s.disjointWith(sprime), "buildEquality");

    BDD result = home.bddOne();
    for (size_t att = 0; att < s.arity(); att++) {
      const Attribute& as = s.asColumnDomains()[att];
      const Attribute& asp = sprime.asColumnDomains()[att];
      Assert<ArityMismatch>(as.reprSize() == asp.reprSize(),
                            "buildEquality(Schema,Schema)");

      auto vecAs = as.asVectorOfVariables(home);
      auto vecAsp = asp.asVectorOfVariables(home);

      BDD p = home.bddOne();
      for (size_t i = 0; i < vecAs.size(); i++) {
        BDD a = ~(vecAs[i] ^ vecAsp[i]);
        p = a & p;
      }
      result = p & result;
    }
    return result;
  }

  static AttributeMap buildMap(const SpaceManager& home, const Schema& relSch,
                               const Schema& toAbstract, const Schema& extra) {
    AttributeMap map;
    for (size_t i = 0; i < toAbstract.arity(); i++) {
      const Attribute& a = toAbstract.asColumnDomains()[i];
      const Attribute& b = extra.asColumnDomains()[i];
      map.add(a, b);
    }
    Schema remain = relSch.differenceWith(home, toAbstract);
    for (size_t i = 0; i < remain.arity(); i++) {
      const Attribute& a = remain.asColumnDomains()[i];
      map.add(a, a);
    }
    return map;
  }
};
}

//////////////
// Relation //
//////////////
namespace CuddAbstraction {

//////////////////
// Constructors //
//////////////////
inline Relation::Relation(void)
    : schema_()
    , data_() {}

inline Relation::Relation(BDD f, const Schema& sch)
    : schema_(sch)
    , data_(f) {}

// TODO: this constructor should disappear and the one below used instead.
inline Relation::Relation(const SpaceManager& home, const Schema& sch)
    : schema_(sch)
    , data_(home.bddZero()) {}

inline Relation::Relation(const SpaceManager& home,
                          const std::vector<Attribute>& sch)
    : schema_(home, sch)
    , data_(home.bddZero()) {}

inline Relation::Relation(const Relation& r)
    : schema_(r.schema_)
    , data_(r.data_) {}

inline Relation& Relation::operator=(const Relation& right) {
  LOG_OPERATION_NARGS("Rel::assign", *this, right);
  if (this != &right) {
    if (!sameSchemaAs(right))
      throw InvalidSchema("Relation::operator=");
    data_ = right.data_;
    schema_ = right.schema_;
  }
  return *this;
}

inline Relation::operator BDD(void) const { return data_; }

inline const Schema& Relation::schema(void) const { return schema_; }

inline void Relation::add(const SpaceManager& home, const NumericTuple& t) {
  if (t.size() != schema_.arity())
    throw ArityMismatch("Relation::add(NumericTuple)");
  BDD tuple = schema_.asBDD(home, t);
  data_ += tuple;
}

namespace internal {
inline BDD encode(const SpaceManager& home, const Schema& dom) {
  return home.bddOne();
}

template <typename... Args>
BDD encode(const SpaceManager& home, const Schema& dom,
           const IntegerDatum& elem, Args... args) {
  // get the attribute that corresponds to elem
  auto c = dom.arity() - sizeof...(Args)-1;
  const Attribute& d = dom.asColumnDomains()[c];
  BDD e = d.asBDD(home, elem);
  BDD r = e * encode(home, dom, args...);
  return r;
}
}

template <typename... Args>
void Relation::add(const SpaceManager& home, Args... args) {
  Assert<ArityMismatch>(sizeof...(Args) == schema_.arity(),
                        "Relation::add(Args...)");
  data_ += internal::encode(home, schema_, args...);
}

template <typename It>
void Relation::addFromIterator(const SpaceManager& home, It begin, It end) {
  LOG_OPERATION_NARGS("Rel::addIter");
  int i = 0;
  BDD tuple = home.bddOne();
  while (begin != end) {
    const Attribute& d = schema_.asColumnDomains().at(i);
    const auto& elem = *(begin);
    BDD e = d.asBDD(home, elem);
    tuple = tuple * e;
    ++begin;
    ++i;
  }

  if (schema_.arity() != (static_cast<size_t>(i)))
    throw ArityMismatch("Relation::addFromIterator");

  data_ += tuple;
}

inline int Relation::readFromFile(const SpaceManager& home, const char* fname) {
  std::ifstream input(fname);
  Assert<GenericException>(static_cast<bool>(input), "Relation::readFromFile");

  int lineNum = 0;
  std::string line;
  while (std::getline(input, line)) {
    std::stringstream is(line);
    addFromIterator(home, std::istream_iterator<int>(is),
                    std::istream_iterator<int>());
    lineNum++;
  }
  return lineNum;
}

inline void Relation::writeToFile(const SpaceManager& home,
                                  const char* fname) const {
  std::ofstream output(fname);
  Assert<GenericException>(static_cast<bool>(output), "Relation::writeToFile");

  for (const NumericTuple& tuple : asTuples(home)) {
    for (auto it = tuple.begin(); it != tuple.end();) {
      output << static_cast<int>(*it);
      ++it;
      if (it != tuple.end())
        output << " ";
    }
    output << std::endl;
  }
}

inline void Relation::remove(const SpaceManager& home, const NumericTuple& t) {
  Assert<ArityMismatch>(t.size() == schema_.arity(), "Relation::remove("
                                                     "NumericTuple&)");
  BDD tuple = schema_.asBDD(home, t);
  data_ -= tuple;
}

template <typename... Args>
void Relation::remove(const SpaceManager& home, Args... args) {
  Assert<ArityMismatch>(sizeof...(Args) == schema_.arity(),
                        "Relation::remove(Args...)");
  data_ -= internal::encode(home, schema_, args...);
}

////////////////////
// Set operations //
////////////////////
inline Relation Relation::unionWith(const Relation& r) const {
  Assert<InvalidSchema>(sameSchemaAs(r), "Relation::unionWith");
  return Relation(data_ + r.data_, schema_);
}

inline Relation Relation::intersectWith(const Relation& r) const {
  Assert<InvalidSchema>(sameSchemaAs(r), "Relation::intersectWith");
  return Relation(data_ * r.data_, schema_);
}

inline Relation Relation::differenceWith(const Relation& r) const {
  Assert<InvalidSchema>(sameSchemaAs(r), "Relation::differenceWith");
  return Relation(data_ - r.data_, schema_);
}

inline Relation Relation::complement(void) const {
  return Relation(~data_, schema_);
}

inline bool Relation::isEmpty(void) const {
  return SpaceManager::isFalse(data_);
}

inline bool Relation::isFull(void) const { return SpaceManager::isTrue(data_); }

inline bool Relation::isSubsetOf(const Relation& r) const {
  Assert<InvalidSchema>(sameSchemaAs(r), "Relation::isSubsetOf");
  return data_ <= r.data_;
}

inline bool Relation::isDisjointWith(const Relation& r) const {
  LOG_OPERATION_NARGS("Rel::disj", *this, r);
  Assert<InvalidSchema>(sameSchemaAs(r), "Relation::isDisjointWith");
  // An straightforward way to see if two relations are disjoint is by computing
  // the intersection and then testing it against the empty relation. That is:
  // return intersectWith(r).isEmpty();

  // However, intersection operation relies on conjunction of BDDs which has
  // complexity O(|this|*|r|). A cheaper way to test disjointness is by testing
  // if @a r is a subset of the complement of this relation. By definition the
  // relation and its complements are disjoint. Testing for subset is cheaper
  // than computing the conjunction of the BDDs.
  return r.data_ <= !data_;
}

inline double Relation::cardinality(void) const {
  LOG_OPERATION_NARGS("Rel::card");
  // The number of variables in the BDD representing the function is the same as
  // the variables that represent its attributes.
  int vars = schema_.asCube().SupportSize();
  return data_.CountMinterm(vars);
}

inline double Relation::maxCardinality(void) const {
  LOG_OPERATION;
  int vars = schema_.asCube().SupportSize();
  // TODO: compute the representation of a full relation this way we avoid
  // taking the SpaceManager as parameter. Hopefully this is optimized by the
  // BDD library but I have to check that.
  BDD full = data_ + ~data_;
  return full.CountMinterm(vars);
}

inline size_t Relation::arity(void) const {
  LOG_OPERATION_NARGS("Rel::arity");
  return schema_.arity();
}

inline bool Relation::sameAs(const Relation& r) const {
  LOG_OPERATION_NARGS("Rel::equal", *this, r);
  if (!sameSchemaAs(r))
    return false;
  return data_ == r.data_;
}

inline bool Relation::sameSchemaAs(const Relation& r) const {
  return schema_ == r.schema_;
}

inline Relation Relation::withSchema(const SpaceManager& home,
                                     const AttributeMap& m,
                                     const Schema& s) const {
  LOG_OPERATION_NARGS("Rel::rename", *this, s);
  Assert<InvalidAttributeMap>(m.mappingBetween(schema_, s),
                              "Relation::withSchema");
  if (m.isIdentity())
    return Relation(*this);

  std::vector<BDD> left, right;
  std::tie(left, right) = m.BDDMapping(home);
  BDD newFunc = data_.SwapVariables(left, right);
  return Relation(newFunc, s);
}

inline Relation Relation::projectTo(const SpaceManager& home,
                                    const Schema& newSchema) const {
  LOG_OPERATION_NARGS("Rel::project", *this, newSchema);
  if (schema_ == newSchema)
    return Relation(*this);
  // TODO: change to use Assert<>
  if (!newSchema.isEmpty() && schema_.disjointWith(newSchema))
    throw ExpectingContainedDomains("Relation::projectTo");

  Schema q = schema_.differenceWith(home, newSchema);
  BDD quantify = q.asCube();
  BDD newFunc = data_.ExistAbstract(quantify);

  return Relation(newFunc, newSchema);
}

inline Relation
Relation::uniqueAbstract(const SpaceManager& home,
                         const Schema& attributesToAbstract) const {
  LOG_OPERATION_ARGS(*this, attributesToAbstract);
  Assert<ExpectingContainedDomains>(schema_.contains(attributesToAbstract),
                                    "Relation::uniqueAbstract");

  Schema newSchema = schema_.differenceWith(home, attributesToAbstract);
  BDD newFunc = home.uniqueAbstract(data_, attributesToAbstract.asCube());
  return Relation(newFunc, newSchema);
}

inline Relation Relation::uniqueAbstract(
    const SpaceManager& home,
    const std::vector<Attribute>& attributesToAbstract) const {
  LOG_OPERATION_ARGS(*this, attributesToAbstract);

  return uniqueAbstract(home, Schema(home, attributesToAbstract));
}

inline Relation Relation::uniqueAbstractFast(
    const SpaceManager& home,
    const std::vector<Attribute>& attributesToAbstract) const {
  Schema schemaToAbstract(home, attributesToAbstract);
  Assert<ExpectingContainedDomains>(schema_.contains(schemaToAbstract),
                                    "Relation::uniqueAbstractFast");

  Schema newSchema = schema_.differenceWith(home, schemaToAbstract);
  BDD newFunc = home.uniqueAbstractFast(data_, schemaToAbstract.asCube());
  return Relation(newFunc, newSchema);
}

inline bool Relation::canUseUniqueAbstractFast(
    const SpaceManager& home,
    const std::vector<Attribute>& attributesToAbstract) const {
  Schema schemaToAbstract(home, attributesToAbstract);
  Assert<ExpectingContainedDomains>(schema_.contains(schemaToAbstract),
                                    "Relation::canUseUniqueAbstractFast");

  Schema nonAbstracted = schema_.differenceWith(home, schemaToAbstract);
  bool result = home.canUseUniqueAbstractFast(nonAbstracted.asCube(),
                                              schemaToAbstract.asCube());
  return result;
}

inline bool
Relation::canUseFastUniqueWithRespectTo(const SpaceManager& home,
                                        const Schema& attributes) const {
  Schema abstract = schema_.differenceWith(home, attributes);
  return canUseUniqueAbstractFast(home, abstract.asColumnDomains());
}

inline Relation
Relation::uniqueWithRespectTo(const SpaceManager& home,
                              const Schema& attributes,
                              bool useFastUniqueAbstract) const {
  Assert<ExpectingContainedDomains>(schema_.contains(attributes),
                                    "Relation::uniqueWithRespectTo");
  /*
  fmt::print_colored(fmt::RED, "Fast unique abstract: {}\n",
                     useFastUniqueAbstract);
  */
  Schema abstract = schema_.differenceWith(home, attributes);
  BDD uniqueAbs = home.bddZero();

#ifndef NDEBUG
  bool f = canUseUniqueAbstractFast(home, abstract.asColumnDomains());
  if (f != useFastUniqueAbstract)
    std::cerr << "Detected incorrect value of useFastUniqueAbstract\n";
#endif

  if (useFastUniqueAbstract)
    uniqueAbs = home.uniqueAbstractFast(data_, abstract.asCube());
  else
    uniqueAbs = home.uniqueAbstract(data_, abstract.asCube());

  return Relation(data_ * uniqueAbs, schema_);
}

inline Relation
Relation::uniqueWithRespectTo(const SpaceManager& home,
                              const std::vector<Attribute>& attributes,
                              bool useFastUniqueAbstract) const {
  return uniqueWithRespectTo(home, Schema(home, attributes),
                             useFastUniqueAbstract);
}

inline UniqueAbstractor Relation::createUniqueAbstractor(
    SpaceManager& home, const std::vector<Attribute>& attributes) const {
  return UniqueAbstractor(home, schema(), attributes);
}

inline Relation
Relation::uniqueWithRespectTo(const SpaceManager& home,
                              const UniqueAbstractor& abstractor) const {
  Relation rPrime = abstractor.relOnTempAttributes(home, *this);
  BDD bddRPrime = rPrime.data_;
  BDD result =
      data_ &
      ~(bddRPrime.AndAbstract(abstractor.disequality(), abstractor.extra()));
  return Relation(result, schema());
}

inline Relation Relation::existAbstract(const SpaceManager& home,
                                        const Schema& attributes) const {
  LOG_OPERATION_ARGS(*this, attributes);
  Assert<ExpectingContainedDomains>(schema_.contains(attributes),
                                    "Relation::existAbstract");
  BDD newFunc = data_.ExistAbstract(attributes.asCube());
  return Relation(newFunc, schema_);
}

inline Relation Relation::crossWith(const SpaceManager& home,
                                    const Schema& sch) const {
  LOG_OPERATION_NARGS("Rel::crossSch", *this, sch);
  Assert<ExpectingDisjointSchemas>(schema_.disjointWith(sch),
                                   "Relation::crossWith(Schema)");

  Schema newDom = schema_.unionWith(home, sch);
  return Relation(data_, newDom);
}

inline Relation Relation::crossWith(const SpaceManager& home,
                                    const Relation& r) const {
  LOG_OPERATION_NARGS("Rel::crossRel", *this, r);
  Assert<ExpectingDisjointSchemas>(schema_.disjointWith(r.schema()),
                                   "Relation::crossWith(Relation)");

  Schema newDom = schema_.unionWith(home, r.schema());
  return Relation(data_ * r.data_, newDom);
}

inline Relation Relation::joinWith(const SpaceManager& home,
                                   const Relation& r) const {
  LOG_OPERATION_NARGS("Rel::join", *this, r);
  // TO_OPTIMIZE: Provide an operation that does not require the new schema to
  // be computed but is passed as an argument instead.

  // TODO: domain computation can take some time and can be optimized by storing
  // the data somewhere else.
  if (schema() != r.schema()) {
    Schema newDom = schema_.unionWith(home, r.schema());
    return Relation(data_ * r.data_, newDom);
  } else {
    return Relation(data_ * r.data_, schema());
  }
}

inline Relation Relation::joinWithAndProject(const SpaceManager& home,
                                             const Relation& r,
                                             const Schema& project) const {
  Schema joinSchema = schema_.unionWith(home, r.schema());
  Assert<ExpectingContainedDomains>(joinSchema.contains(project),
                                    "Relation::joinWithAndProject");
  Schema toAbstract = joinSchema.differenceWith(home, project);
  BDD result = data_.AndAbstract(r.data_, toAbstract.asCube());
  return Relation(result, project);
}

inline Relation Relation::composeWith(const SpaceManager& home,
                                      const Relation& r) const {
  LOG_OPERATION_ARGS(*this, r);
  // TODO: Computing the domains of the resulting relation and the attributes on
  // which to compose can impact performance. This can be improved by storing
  // them somewhere else and just making the and of the BDD followed by the
  // abstraction
  Schema composeOn = schema_.intersectWith(home, r.schema());

  Schema resultDomain =
      schema_.unionWith(home, r.schema()).differenceWith(home, composeOn);

  return Relation(data_.AndAbstract(r.data_, composeOn.asCube()), resultDomain);
}

inline std::vector<NumericTuple>
Relation::asTuples(const SpaceManager& home) const {
  LOG_OPERATION_ARGS(*this);
  return schema_.asTuples(home, data_);
}

///////////////
// Selection //
///////////////
inline NumericTuple Relation::pickRandomTuple(void) const {
  LOG_OPERATION_ARGS(*this);
  Assert<SelectionOnEmptyRelation>(!isEmpty(),
                                   "Relation::pickAnyUnarySubrelation");
  // TODO: this operation can be replaced by the one that returns one minterm in
  // CUDD
  int* cube;
  CUDD_VALUE_TYPE value;
  data_.FirstCube(&cube, &value);

  // Convert the cube into an assignment by replacing don't cares by 0's
  for (const auto& d : schema_.asColumnDomains()) {
    for (auto idx : d.getIndices()) {
      if (cube[idx] == 2)
        cube[idx] = 0;
    }
  }
  return schema_.decodeAssignment(cube);
}

inline Relation
Relation::pickAnyUnarySubrelation(const SpaceManager& home) const {
  LOG_OPERATION_NARGS("Rel::pickAnyUnarySub", *this);
  Assert<SelectionOnEmptyRelation>(!isEmpty(),
                                   "Relation::pickAnyUnarySubrelation");
  return Relation(data_.PickOneMinterm(home.cubeToVector(schema_.asCube())),
                  schema_);
}

inline Relation
Relation::pickRandomSubrelation(const SpaceManager& home) const {
  LOG_OPERATION_ARGS(*this);
  Assert<SelectionOnEmptyRelation>(!isEmpty(),
                                   "Relation::pickRandomSubrelation");

  char* string = new char[home.ReadSize()];
  data_.PickOneCube(string);

  // Transform string into a BDD
  BDD subrel = home.bddOne();
  for (const Attribute& att : schema_.asColumnDomains()) {
    for (auto idx : att.getIndices()) {
      BDD v = home.bddVar(idx);
      if (string[idx] == 0)
        v = !v;
      subrel &= v;
    }
  }
  Assert<GenericException>(subrel <= data_,
                           "Produced subrelation is not a subset!!");
  return Relation(subrel, schema_);
}

inline Relation Relation::pickRandomSubrelation(const SpaceManager& home,
                                                size_t n) const {
  LOG_OPERATION_ARGS(*this);
  if (cardinality() <= n)
    return *this;

  Assert<SelectionOnEmptyRelation>(!isEmpty(),
                                   "Relation::pickRandomSubrelation");

  if (n > maxCardinality())
    return *this;

  // Prepare the arguments to call Cudd_bddPickArbitraryMinterms that does not
  // have a C++ counterpart.
  BDD varsInSchema = schema_.asCube();
  std::vector<BDD> vars = home.cubeToVector(varsInSchema);

  DdNode** v = (DdNode**)malloc(sizeof(DdNode*) * vars.size());
  for (size_t i = 0; i < vars.size(); i++)
    v[i] = vars[i].getNode();

  DdNode** result = Cudd_bddPickArbitraryMinterms(
      data_.manager(), data_.getNode(), v, vars.size(), n);

  Assert<CUDDUnexpected>(
      result != NULL,
      "Relation::pickRandomSubrelation : Cudd_bddPickArbitraryMinterms");

  // Aggregate all the minterms into one BDD representing the subrelation
  BDD subrelation = home.bddZero();
  for (size_t i = 0; i < n; i++)
    subrelation += BDD(home, result[i]);

  free(result);
  free(v);
  Assert<GenericException>(subrelation <= data_,
                           "Resulting relation is not a subrelation!");
  return Relation(subrelation, schema_);
}

inline Relation
Relation::pickRandomSubrelation(const SpaceManager& home,
                                const std::vector<Attribute>& a) const {
  LOG_OPERATION_ARGS(*this);

  Assert<SelectionOnEmptyRelation>(!isEmpty(),
                                   "Relation::pickRandomSubrelation");

  // TODO: Assert that a is a subset of the schema of this relation

  // Prepare the arguments to call Cudd_SubsetWithMaskVars
  // 1) DdNode** version of the BDD variables in the schema
  std::vector<BDD> varsInSchema = home.cubeToVector(schema_.asCube());
  DdNode** v = (DdNode**)malloc(sizeof(DdNode*) * varsInSchema.size());
  for (size_t i = 0; i < varsInSchema.size(); i++)
    v[i] = varsInSchema[i].getNode();

  // 2) DdNode** version of the BDD variables of a.
  size_t varsInMaskedSize = 0;
  for (const Attribute& att : a)
    varsInMaskedSize += att.reprSize();

  std::vector<BDD> varsInMasked;
  varsInMasked.reserve(varsInMaskedSize);
  for (const Attribute& att : a) {
    std::vector<BDD> cubeOfAtt = home.cubeToVector(att.asCube());
    varsInMasked.insert(end(varsInMasked), begin(cubeOfAtt), end(cubeOfAtt));
  }

  DdNode** m = (DdNode**)malloc(sizeof(DdNode*) * varsInMaskedSize);
  for (size_t i = 0; i < varsInMaskedSize; i++)
    m[i] = varsInMasked[i].getNode();

  // Actual call to Cudd_SubsetWithMaskedVars
  DdNode* subrel =
      Cudd_SubsetWithMaskVars(data_.manager(), data_.getNode(), v,
                              varsInSchema.size(), m, varsInMaskedSize);
  Assert<CUDDUnexpected>(subrel != NULL,
                         "Cudd_SubsetWithMaskedVars returned NULL");
  free(v);
  free(m);

  BDD result(home, subrel);
  return Relation(result, schema_);
}

inline Relation Relation::createEmpty(const SpaceManager& home,
                                      const Schema& sch) {
  return Relation(home, sch);
}

inline Relation Relation::createFull(const SpaceManager& home,
                                     const Schema& sch) {
  return Relation(home.bddOne(), sch);
}

inline void Relation::toDot(const SpaceManager& home, const char* fileName,
                            const char* funcName, bool asADD) const {
  LOG_OPERATION_ARGS(*this);
  DdNode* data = asADD ? data_.Add().getNode() : data_.getNode();
  home.toDot(fileName, data, funcName, schema().levelNames(home));
}

inline RelationStats Relation::getStats(void) const {
  return RelationStats(*this);
}

inline void Relation::bddInspect(const std::string& label,
                                 bool printCover) const {
  auto vars = schema_.asCube().SupportSize();
  auto minterms = data_.CountMinterm(vars);
  auto nodes = data_.nodeCount();

  fmt::print("{}:  {} nodes, {} minterms\n", label, nodes, minterms);
  if (printCover)
    data_.PrintCover();
}

inline bool operator==(const Relation& lhs, const Relation& rhs) {
  return lhs.sameAs(rhs);
}
}
#endif