# Relation representation using BDDs

## Introduction

This project consists of a layer on top of the [CUDD](http://vlsi.colorado.edu/~fabio/CUDD/) library that implements the concept of mathematical relations and operations on them.

## Installation
In order to build the library from sources you will need a recent C++ compiler and the [CMake](http://www.cmake.org/) configuration tool. Assuming the sources are located in the directory `/home/user/cudd-abstraction` perform the following steps.

```
$ cd /home/user/
$ mkdir build-cudd-abstraction
$ cd build-cudd-abstraction
$ cmake ../cudd-abstraction/
$ make
```

After the build process is done you can run the tests using `./abstraction/tests/test`. Additionaly you can have a look at the test files to see how the library is used. 

Additional information can be passed to `cmake` in order to specify custom compilers or paths. For instance, on Mac OS you can use:

```
$ cmake -DCMAKE_C_COMPILER=`xcrun -find clang` -DCMAKE_CXX_COMPILER=`xcrun -find clang++`  ../cudd-abstraction
```

## License
The library includes [CUDD](http://vlsi.colorado.edu/~fabio/CUDD/). A copy of the license can be found in `cudd-abstraction/cudd/LICENSE`.

```
Copyright © 2012, Gustavo Gutiérrez
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

 *  Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *  Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
```