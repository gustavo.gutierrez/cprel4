/*
 *  Authors:
 *    Gustavo Gutierrez-Sabogal <gustavo.ggutierrez@gmail.com>
 *
 *  Copyright:
 *    Gustavo Gutierrez, 2013
 *
 *  Permission is hereby granted, free of charge, to any person obtaining
 *  a copy of this software, to deal in the software without restriction,
 *  including without limitation the rights to use, copy, modify, merge,
 *  publish, distribute, sublicense, and/or sell copies of the software,
 *  and to permit persons to whom the software is furnished to do so, subject
 *  to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 *  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 *  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#include "rel.hh"
#include <gecode/search.hh>
#include <gecode/gist.hh>

using namespace MPG;
using CuddAbstraction::Attribute;
using CuddAbstraction::AttributeDomain;
using CuddAbstraction::makeDomain;
using CuddAbstraction::DomainGroup;
using CuddAbstraction::insert;

class KColor : public Gecode::Space {
protected:
  RelVar coloring;
  RelVar forbColoring;

public:
  KColor(const Attribute& source, const Attribute& target,
         const Attribute& color, const Relation& neighbor,
         const Relation& colors, const Relation& countries) {

    auto& relHome = MPG::Rel::getRelationSpace();

    // coloring: target -> color
    {
      Relation maxColoring = countries.crossWith(relHome, colors);
      coloring = RelVar(*this, empty(maxColoring.schema()), maxColoring);
    }

    // forbColoring: source -> color
    Schema forbColoringSch(relHome, {source, color});
    forbColoring = RelVar(*this, forbColoringSch);

    RelVar adj(*this, neighbor, neighbor);

    // First constraint
    compose(*this, adj, coloring, forbColoring);

    Schema compatForbSch(relHome, {target, color});
    RelVar compatForb(*this, compatForbSch);

    // forbColoring and compatForb are the same relation modulo the following
    // mapping of attributes:
    // - source is target
    // - color is color
    rename(*this, forbColoring, {{source, target}, {color, color}}, compatForb);
    disjoint(*this, coloring, compatForb);

    // Second constraint
    RelVar countriesRel(*this, countries, countries);
    projection(*this, coloring, countries.schema(), countriesRel);

    // Branching
    branch(*this, coloring, REL_VAL_MIN());
  }

  void print(std::ostream& os, DomainGroup& dg) const {
#ifdef GECODE_HAS_GIST
    HtmlOutput(os);
    Rel::print(os, dg, coloring);
#else
    Rel::print(os, dg, coloring);
#endif
  }

  KColor(bool share, KColor& s) : Gecode::Space(share, s) {
    coloring.update(*this, share, s.coloring);
    forbColoring.update(*this, share, s.forbColoring);
  }
  virtual Space* copy(bool share) { return new KColor(share, *this); }
};

int main(int argc, char* argv[]) {

  auto& relHome = MPG::Rel::getRelationSpace();
  {
    // Domain of countries
    AttributeDomain countryDom = makeDomain(8);
    // Domain of colors
    AttributeDomain colorDom = makeDomain(4);

    Attribute source(relHome, countryDom, "source"),
        target(relHome, countryDom, "target"),
        color(relHome, colorDom, "color");
    DomainGroup dg;
    dg << source << target << color;

    Relation neighbor(relHome, {source, target});
    {
      insert(relHome, dg, neighbor, "WA", "NT");
      insert(relHome, dg, neighbor, "WA", "SA");
      insert(relHome, dg, neighbor, "NT", "WA");
      insert(relHome, dg, neighbor, "NT", "SA");
      insert(relHome, dg, neighbor, "NT", "Q");
      insert(relHome, dg, neighbor, "Q", "NT");
      insert(relHome, dg, neighbor, "Q", "SA");
      insert(relHome, dg, neighbor, "Q", "NSW");
      insert(relHome, dg, neighbor, "NSW", "Q");
      insert(relHome, dg, neighbor, "NSW", "SA");
      insert(relHome, dg, neighbor, "NSW", "V");
      insert(relHome, dg, neighbor, "V", "NSW");
      insert(relHome, dg, neighbor, "V", "SA");
      insert(relHome, dg, neighbor, "SA", "WA");
      insert(relHome, dg, neighbor, "SA", "NT");
      insert(relHome, dg, neighbor, "SA", "Q");
      insert(relHome, dg, neighbor, "SA", "NSW");
      insert(relHome, dg, neighbor, "SA", "V");
    }

    Relation colors(relHome, {color});
    {
      insert(relHome, dg, colors, "red");
      insert(relHome, dg, colors, "green");
      insert(relHome, dg, colors, "blue");
    }

    Relation countries(relHome, {target});
    {
      insert(relHome, dg, countries, "WA");
      insert(relHome, dg, countries, "NT");
      insert(relHome, dg, countries, "Q");
      insert(relHome, dg, countries, "NSW");
      insert(relHome, dg, countries, "V");
      insert(relHome, dg, countries, "SA");
    }

    KColor* g = new KColor(source, target, color, neighbor, colors, countries);
#ifdef GECODE_HAS_GIST
    Rel::PrintDomainGroup<KColor> p("Solution", dg);
    Gecode::Gist::Options o;
    o.inspect.solution(&p);
    Gecode::Gist::dfs(g, o);
    delete g;
#else
    Gecode::DFS<KColor> e(g);
    delete g;
    unsigned int solutions = 0;
    while (Gecode::Space* s = e.next()) {
      solutions++;
      std::cerr << "-> " << solutions << "\n";
      static_cast<KColor*>(s)->print(std::cerr, dg);
      delete s;
    }
    std::cout << "Solutions: " << solutions << std::endl;
#endif
  }
  std::cout << "References: " << relHome.zeroReferences() << std::endl;
  return 0;
}
