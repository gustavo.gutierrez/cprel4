/*
 *  Authors:
 *    Gustavo Gutierrez-Sabogal <gustavo.ggutierrez@gmail.com>
 *
 *  Copyright:
 *    Gustavo Gutierrez, 2013
 *
 *  Permission is hereby granted, free of charge, to any person obtaining
 *  a copy of this software, to deal in the software without restriction,
 *  including without limitation the rights to use, copy, modify, merge,
 *  publish, distribute, sublicense, and/or sell copies of the software,
 *  and to permit persons to whom the software is furnished to do so, subject
 *  to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 *  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 *  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#include "rel.hh"
#include <gecode/int.hh>
#include <gecode/search.hh>
#include <gecode/gist.hh>

using CuddAbstraction::Attribute;
using CuddAbstraction::makeDomain;

using Gecode::IntVar;
// using Gecode::IntSet;

using namespace MPG;

class Cardinality : public Gecode::Space {
protected:
  RelVar x;
  IntVar y;

public:
  Cardinality(const Relation& xl0, const Relation& xg0, int minCard,
              int maxCard)
      : x(*this, xl0, xg0)
      , y(*this, minCard, maxCard) {
    cardinality(*this, x, y);
    branch(*this, x, REL_VAL_MIN());
    branch(*this, y, Gecode::INT_VAL_MIN());
  }

  void print(std::ostream& os) const {
    os << "X:" << x << std::endl;
    os << "Y:" << y << std::endl;
  }
  Cardinality(bool share, Cardinality& s) : Gecode::Space(share, s) {
    x.update(*this, share, s.x);
    y.update(*this, share, s.y);
  }
  virtual Space* copy(bool share) { return new Cardinality(share, *this); }
};

int main(int argc, char* argv[]) {
  auto& relHome = MPG::Rel::getRelationSpace();
  {
    Attribute c0(relHome, makeDomain(4));

    Schema cDom(relHome, {c0});

    Cardinality* g = new Cardinality(Relation::createEmpty(relHome, cDom),
                                     Relation::createFull(relHome, cDom), 2, 4);
#ifdef GECODE_HAS_GIST
    Gecode::Gist::Print<Cardinality> p("Solution");
    Gecode::Gist::Options o;
    o.inspect.click(&p);
    Gecode::Gist::dfs(g, o);
    delete g;
#else
    Gecode::DFS<Cardinality> e(g);
    delete g;
    unsigned int solutions = 0;
    while (Gecode::Space* s = e.next()) {
      solutions++;
      // static_cast<Cardinality*>(s)->print();
      delete s;
    }
    std::cout << "Solutions: " << solutions << std::endl;
#endif
  }
  std::cout << "References: " << relHome.zeroReferences() << std::endl;
  return 0;
}
