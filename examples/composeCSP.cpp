/*
 *  Authors:
 *    Gustavo Gutierrez-Sabogal <gustavo.ggutierrez@gmail.com>
 *
 *  Copyright:
 *    Gustavo Gutierrez, 2013
 *
 *  Permission is hereby granted, free of charge, to any person obtaining
 *  a copy of this software, to deal in the software without restriction,
 *  including without limitation the rights to use, copy, modify, merge,
 *  publish, distribute, sublicense, and/or sell copies of the software,
 *  and to permit persons to whom the software is furnished to do so, subject
 *  to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 *  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 *  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#include "rel.hh"
#include <gecode/search.hh>
#include <gecode/gist.hh>

using namespace MPG;

class TrivialCSP : public Gecode::Space {
protected:
  RelVar x;
  RelVar y;
  RelVar z;

public:
  TrivialCSP(const Relation& xl0, const Relation& xg0, const Schema& ySch,
             const Schema& zSch)
      : x(*this, xl0, xg0)
      , y(*this, empty(ySch), full(ySch))
      , z(*this, empty(zSch), full(zSch)) {

    std::cerr << "Before posting constraints\n"
              << "Relation var x\n" << x << "\n";

    compose(*this, x, y, z);

    // Branching
    RelVarArgs b;
    b << x;
    b << y;
    b << z;
    Rnd r(1U);
    branch(*this, b, REL_VAR_RND(r), REL_VAL_MIN());
    // branch(*this,x,REL_VAL_MIN());
    // branch(*this,y,REL_VAL_MIN());
    // branch(*this,z,REL_VAL_MIN());
  }

  void print(std::ostream& os) const {
#ifdef GECODE_HAS_GIST
    // os << CuddAbstraction::setHTMLPrinting;
    os << "<table align=\"center\" cellpadding=\"6\">";
    os << "<tr><th>X</th><th>Y</th><th>Z</th></tr>";
    os << "<tr><td>";
    os << x;
    os << "</td><td>";
    os << y;
    os << "</td><td>";
    os << z;
    os << "</td></tr></table>";
#else
    // os << CuddAbstraction::setRawPrinting;
    os << "X" << std::endl << x << std::endl;
    os << "Y" << std::endl << y << std::endl;
    os << "Z" << std::endl << z << std::endl;
#endif
  }

  TrivialCSP(bool share, TrivialCSP& s) : Gecode::Space(share, s) {
    x.update(*this, share, s.x);
    y.update(*this, share, s.y);
    z.update(*this, share, s.z);
  }
  virtual Space* copy(bool share) { return new TrivialCSP(share, *this); }
};

int main(int argc, char* argv[]) {
  using CuddAbstraction::Attribute;
  using CuddAbstraction::makeDomain;

  auto& relHome = MPG::Rel::getRelationSpace();
  {
    Attribute a(relHome, makeDomain(4), "a"), b(relHome, makeDomain(4), "b"),
        i(relHome, makeDomain(4), "i");

    Schema xDom = schema({a, i});
    Schema yDom = schema({i, b});
    Schema zDom = schema({a, b});

    Relation maxX(relHome, xDom);
    maxX.add(relHome, 1, 0);
    maxX.add(relHome, 2, 0);

    // printRelation(relHome, maxX);
    TrivialCSP* g = new TrivialCSP(empty(xDom), maxX, yDom, zDom);
#ifdef GECODE_HAS_GIST
    Gecode::Gist::Print<TrivialCSP> p("Solution");
    Gecode::Gist::Options o;
    o.inspect.click(&p);
    Gecode::Gist::dfs(g, o);
    delete g;
#else
    Gecode::DFS<TrivialCSP> e(g);
    delete g;
    unsigned int solutions = 0;
    while (Gecode::Space* s = e.next()) {
      solutions++;
      // static_cast<TrivialCSP*>(s)->print();
      delete s;
    }
    std::cout << "Solutions: " << solutions << std::endl;
#endif
  }
  std::cout << "References: " << relHome.zeroReferences() << std::endl;
  return 0;
}
