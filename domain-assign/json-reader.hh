#ifndef __JSON_READER_HH__
#define __JSON_READER_HH__

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <cassert>
#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <forward_list>

#include "domain-assign-support.hh"

using boost::property_tree::ptree;

typedef std::map<std::string, Relation> RelationMap;
typedef std::forward_list<Operation> OperationsList;

void extractRelation(ptree& rel, RelationMap& m) {
  Relation r(rel.get<std::string>("Name"));
  for (auto& col : rel.get_child("Columns"))
    r.addColumn(col.second.data());
  m[r.getName()] = r;
}

std::pair<std::string, std::string> extractPair(ptree& match) {
  auto it = begin(match);
  std::string first = it->second.data();
  ++it;
  std::string second = it->second.data();
  return std::make_pair(first, second);
}

void extractOperation(ptree& op, RelationMap& relations,
                      OperationsList& operations) {
  std::string kind = op.get<std::string>("Operation");
  std::string name = op.get<std::string>("Name");
  std::string lhs = op.get<std::string>("LHS");
  std::string rhs = op.get<std::string>("RHS");

  const Relation& lhsRel = relations[lhs];
  const Relation& rhsRel = relations[rhs];

  Operation o(kind, name, lhsRel, rhsRel);

  const auto& matchings = op.get_child_optional("Matchings");
  if (matchings) {
    for (auto& m : matchings.get()) {
      auto p = extractPair(m.second);
      o.addColumnsInMatch(p.first, p.second);
    }
  } else {
    assert(kind == "set");
  }
  operations.push_front(o);
}

void readCSP(const char* fname, RelationMap& relations,
             OperationsList& operations) {
  ptree pt;
  read_json(fname, pt);

  for (auto& rel : pt.get_child("Relations")) {
    extractRelation(rel.second, relations);
  }

  for (auto& op : pt.get_child("Operations")) {
    extractOperation(op.second, relations, operations);
  }
}

#endif